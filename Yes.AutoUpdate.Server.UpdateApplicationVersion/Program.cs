﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Yes.AutoUpdate.Server.UpdateApplicationVersion
{
    class Program
    {
        static int Main(string[] args)
        {
            int ExitCode = int.MinValue;

            try
            {
                if (args.Length != 2)
                {
                    ExitCode = 1;
                    return ExitCode;
                }
                int UpdateWaitingMinutes = 0;
                if (!int.TryParse(args[0], out UpdateWaitingMinutes))
                {
                    Console.WriteLine("參數1錯誤，必須為數字。");
                    ExitCode = 1;
                    return ExitCode;
                }

                Yes.AutoUpdate.Server.Business.ApplicationAutoUpdateSettingHelper HelperApplicationAutoUpdateSetting = new Yes.AutoUpdate.Server.Business.ApplicationAutoUpdateSettingHelper();

                string DownloadBaseUrl = ConfigurationManager.AppSettings["DownloadBaseUrl"];
                string[] UpdateBaseFolderPaths = ConfigurationManager.AppSettings["UpdateBaseFolderPaths"].Split(',');
                string VersionFileName = ConfigurationManager.AppSettings["VersionFileName"];
                string[] CheckExistFileNames = ConfigurationManager.AppSettings["VersionFileName"].Split(',');
                string UpdateZipFileName = args[1];
                string ErrorMessage = string.Empty;

                if (!HelperApplicationAutoUpdateSetting.UpdateApplicationVersion(
                    DownloadBaseUrl,
                    UpdateBaseFolderPaths,
                    VersionFileName,
                    CheckExistFileNames,
                    UpdateZipFileName,
                    UpdateWaitingMinutes,
                    out ErrorMessage))
                {
                    Console.WriteLine(ErrorMessage);
                    ExitCode = 1;
                }
                else
                {
                    Console.WriteLine("更新應用程式版本資訊成功。");
                    ExitCode = 0;
                }
            }
            catch
            {

            }
            return ExitCode;
        }
    }
}
