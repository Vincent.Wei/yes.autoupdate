﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Net.Security;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 用戶端版本協助類別。
    /// </summary>
    public class ClientVersionHelper : Base
    {
        #region ReportApplicationVersion
        /// <summary>
        /// 回報應用程式資訊。
        /// 1.從檔案讀取Xml字串。
        /// 2.將Xml字串反序列化。
        /// 3.呼叫WebService回報。
        /// </summary>
        /// <param name="VersionFilePath">版本資訊檔。</param>
        /// <param name="ReportUrl">WebService位置。</param>
        /// <param name="ReportMethod">方法。</param>
        /// <param name="ReportParameter">屬性。</param>
        /// <returns>是否成功。</returns>
        public static bool ReportApplicationVersion(string VersionFilePath, string ReportUrl, string ReportMethod, string ReportParameter)
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName,
                    "VersionFilePath: \"{0}\"; ReportUrl: \"{1}\"; ReportMethod: \"{2}\"; ReportParameter: \"{3}\";",
                    VersionFilePath, ReportUrl, ReportMethod, ReportParameter);

                // Ignore SSL certificate errors
                ServicePointManager.ServerCertificateValidationCallback =
                   new RemoteCertificateValidationCallback(
                       delegate
                       { return true; }
                   );

                if (!File.Exists(VersionFilePath))
                {
                    //檔案不存在。
                    Log.TraceWarning(LogSourceName, (int)MessageId.ClientVersionHelper_LoadApplicationFileNotExistsError, "XML檔案不存在，檔案: \"{0}\"。", VersionFilePath);
                    //Log.LogWarning(LogSourceName, (int)MessageId.Application_LoadApplicationFileNotExistsError, "XML檔案不存在，檔案: \"{0}\"。", VersionFilePath);

                    ReturnResult = false;
                    return ReturnResult;
                }

                //XML反序列化。
                string VersionXML = File.ReadAllText(VersionFilePath);
                Application TempApplication = null;
                try
                {
                    TempApplication = (Application)Base.XmlDeserialize(VersionXML, typeof(Application));
                }
                catch (Exception e)
                {
                    //解析XML失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.ClientVersionHelper_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", VersionFilePath, e);
                    Log.LogError(LogSourceName, (int)MessageId.ClientVersionHelper_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", VersionFilePath, e);
                    ReturnResult = false;
                    return ReturnResult;
                }


                if (TempApplication.IsNeedReportVersion)
                {
                    string ReportParameterFormated = string.Format(ReportParameter,
                        Environment.MachineName,
                        TempApplication.ID.ToString("D"),
                        TempApplication.Name,
                        TempApplication.MajorVersion,
                        TempApplication.MinorVersion);

                    #region WebRequest
                    //建立一個 WebRequest，並設定傳輸方法為 POST    
                    //網址是 WebService的網址，方法是WebService的方法   
                    WebRequest Request = WebRequest.Create(String.Format("{0}/{1}", ReportUrl, ReportMethod));
                    Request.Method = "POST";
                    Request.ContentType = "application/x-www-form-urlencoded";
                    //Request.Timeout = 300;

                    //下面這段是對你要帶的參數編碼(這是用htt ppost傳輸的方式)               
                    //如果WebMetod是 get(int i) 那參數就是 i=10   
                    //如果是 get(string str,int i) 那參數可以是 str=gogo&i=10   
                    byte[] ReportParameterBytes = System.Text.Encoding.UTF8.GetBytes(ReportParameterFormated);
                    Request.ContentLength = ReportParameterBytes.Length;
                    Request.GetRequestStream().Write(ReportParameterBytes, 0, ReportParameterBytes.Length);

                    //取得 WebResponse 的物件 然後把回傳的資料讀出   
                    HttpWebResponse Response = (HttpWebResponse)Request.GetResponse();
                    Stream DataStream = Response.GetResponseStream();
                    StreamReader DataStreamReader = new StreamReader(DataStream);
                    string ResponseContent = DataStreamReader.ReadToEnd();
                    #endregion

                    Log.TraceInformation(LogSourceName, (int)MessageId.ClientVersionHelper_ReportApplicationVersionResponse, "WebService回傳結果: \"{0}\"", ResponseContent);
                }

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.ClientVersionHelper_ReportApplicationVersionException, e, "回報應用程式資訊時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.ClientVersionHelper_ReportApplicationVersionException, e, "回報應用程式資訊時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion
    }
}
