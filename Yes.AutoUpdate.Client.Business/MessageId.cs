﻿using System;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 訊息代碼。
    /// </summary>
    public enum MessageId
    {
        #region AutoUpateProcess (100xx)
        /// <summary>
        /// 自動更新作業時發生例外錯誤。
        /// </summary>
        AutoUpateProcess_AutoUpdateException = 10001,
        /// <summary>
        /// 比對清單檢查是否有新版本時發生例外錯誤。
        /// </summary>
        AutoUpateProcess_CheckNewVersionException = 10002,
        /// <summary>
        /// 檢查重試安裝次數時發生錯誤。
        /// </summary>
        AutoUpdateProcess_CheckRetryFail = 10003,
        /// <summary>
        /// 檢查重試安裝次數時發生例外錯誤。
        /// </summary>
        AutoUpdateProcess_CheckRetryException = 10004,
        /// <summary>
        /// 重試安裝次數已超過最大重試次數。
        /// </summary>
        AutoUpdateProcess_InstallRetryOverMax = 10005,
        /// <summary>
        /// 檢查重試安裝次數的版本比對錯誤。
        /// </summary>
        AutoUpdateProcess_CheckRetryVersionFail = 10006,
        #endregion

        #region Application (200xx)
        /// <summary>
        /// 從檔案讀取應用程式清單時發生例外錯誤。
        /// </summary>
        Application_LoadApplicationListException = 20001,
        /// <summary>
        /// XML檔案不存在，檔案: \"{0}\"。
        /// </summary>
        Application_LoadApplicationFileNotExistsError = 20002,
        /// <summary>
        /// XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。
        /// </summary>
        Application_LoadApplicationXmlDeserializeError = 20003,
        /// <summary>
        /// 從檔案取得應用程式資訊時發生例外錯誤。
        /// </summary>
        Application_LoadApplicationException = 20004,
        /// <summary>
        /// 下載應用程式最新版本並解壓縮至指定目錄發生錯誤，檔案: \"{0}\"。
        /// </summary>
        Application_DownloadNewVersionError = 20005,
        /// <summary>
        /// 下載應用程式最新版本並解壓縮至指定目錄時發生例外錯誤。
        /// </summary>
        Application_DownloadNewVersionException = 20006,
        /// <summary>
        /// 執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。
        /// </summary>
        Application_UpdateVersionError = 20007,
        /// <summary>
        /// 更新應用程式時發生例外錯誤。
        /// </summary>
        Application_UpdateVersionException = 20008,
        /// <summary>
        /// 備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。
        /// </summary>
        Application_BackupVersionError = 20009,
        /// <summary>
        /// 備份原有應用程式檔案時發生例外錯誤。
        /// </summary>
        Application_BackupVersionException = 20010,
        /// <summary>
        /// 備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。
        /// </summary>
        Application_RollbackVersionError = 20011,
        /// <summary>
        /// 還原已備份之應用程式時發生例外錯誤。
        /// </summary>
        Application_RollbackVersionException = 20012,
        /// <summary>
        /// 解壓縮發生錯誤，檔案 \"{0}\" 不存在。
        /// </summary>
        Application_UnzipError = 20013,
        /// <summary>
        /// 解壓縮時發生例外錯誤，檔案: \"{0}\"。
        /// </summary>
        Application_UnzipException = 20014,
        /// <summary>
        /// 寫入應用程式清單檔案時發生例外錯誤。
        /// </summary>
        Application_SaveApplicationListException = 20015,
        /// <summary>
        /// WebService回傳結果: \"{0}\"
        /// </summary>
        Application_ReportApplicationVersionResponse = 20016,
        #endregion

        #region ApplicationUpdateProcess (300xx)
        /// <summary>
        /// 更新程序時發生例外錯誤。
        /// </summary>
        ApplicationUpdateProcess_UpdateApplicationException = 30001,
        /// <summary>
        /// 讀取 \"{0}\" 時發生例外錯誤。
        /// </summary>
        ApplicationUpdateProcess_ReadRetryLogException = 30002,
        /// <summary>
        /// 解析 \"{0}\" 時發生錯誤。
        /// </summary>
        ApplicationUpdateProcess_ParseRetryLogException = 3003,
        /// <summary>
        /// 寫入 \"{0}\" 時發生例外錯誤。
        /// </summary>
        ApplicationUpdateProcess_WriteRetryLogException = 3004,
        /// <summary>
        /// 刪除 \"{0}\" 時發生例外錯誤。
        /// </summary>
        ApplicationUpdateProcess_DeleteRetryLogException = 3005,
        #endregion

        #region ClientVersionHelper (400xx)
        /// <summary>
        /// XML檔案不存在，檔案: \"{0}\"。
        /// </summary>
        ClientVersionHelper_LoadApplicationFileNotExistsError = 40001,
        /// <summary>
        /// XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。"
        /// </summary>
        ClientVersionHelper_LoadApplicationXmlDeserializeError = 40002,
        /// <summary>
        /// WebService回傳結果: \"{0}\"
        /// </summary>
        ClientVersionHelper_ReportApplicationVersionResponse = 40003,
        /// <summary>
        /// 回報應用程式資訊時發生例外錯誤。
        /// </summary>
        ClientVersionHelper_ReportApplicationVersionException = 40004,
        #endregion

        #region BITHelper (500xx)
        /// <summary>
        /// BITS Download Job \"{0}\" 從 \"{1}\" 到 \"{2}\" 時發生例外錯誤。
        /// </summary>
        BITSHelper_BITSDownloadException = 50001,
        /// <summary>
        /// BITS Download 找不到 Job \"{0}\"。
        /// </summary>
        BITSHelper_BITSDownloadJobNotFound = 50002,
        /// <summary>
        /// BITS Download Job \"{0}\" ErrorCode: {1} (\"{2}\")。
        /// </summary>
        BITSHelper_BITSDownloadJobError = 50003,
        #endregion
    }
}
