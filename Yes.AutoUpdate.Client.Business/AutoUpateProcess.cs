﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Data;
using System.Xml;
using Yes.AutoUpdate.Client.Business.AutoUpdateWebService;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Net;
using System.Net.Security;

using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 自動更新程序類別。
    /// </summary>
    public class AutoUpateProcess : Base
    {

        #region Applications
        /// <summary>
        /// 應用程式集合。
        /// </summary>
        private Dictionary<Guid, Application> Applications = null;
        #endregion

        #region ApplicationUpdateProcesses
        /// <summary>
        /// 已執行更新的程序。
        /// </summary>
        private Dictionary<Guid, Thread> ApplicationUpdateProcesses = null;
        #endregion

        #region AutoUpdate
        /// <summary>
        /// 自動更新作業。
        /// 1. 從檔案取得本機程式版本清單。 Application.LoadApplicationList()
        /// 2. 從伺服器取得程式版本清單。 GetApplicationVersion()
        /// 3. 迴圈(檢查版本)
        /// 3.1. 比對清單檢查是否有新版本。 CheckNewVersion()
        /// 3.1.1. 有新版本，建立新Process並執行更新程序。 AutoUpdateProcess
        /// 3.1.2. 無新版本，則繼續下一個迴圈。 continue
        /// 4. 等待所有更新Process結束。(迴圈)
        /// 5. 寫入應用程式清單錯誤並結束程式。 Application.SaveApplicationList()
        /// </summary>
        /// <param name="ApplicationListFileName">應用程式清單檔案名稱。</param>
        /// <returns>是否成功。</returns>
        public bool AutoUpdate(string ApplicationListFileName)
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                // Ignore SSL certificate errors
                ServicePointManager.ServerCertificateValidationCallback =
                   new RemoteCertificateValidationCallback(
                       delegate
                       { return true; }
                   );

                #region 回報目前版本
                string VersionFilePath = GetRealPath(ConfigurationManager.AppSettings["VersionFile"]);
                string ReportUrl = ConfigurationManager.AppSettings["ReportApplicationVersionUrl"];
                string ReportMethod = ConfigurationManager.AppSettings["ReportApplicationVersionMethod"];
                string ReportParameter = ConfigurationManager.AppSettings["ReportApplicationVersionParameter"];

                ClientVersionHelper.ReportApplicationVersion(VersionFilePath, ReportUrl, ReportMethod, ReportParameter);
                #endregion

                Application application = new Application();

                //1. 從檔案取得本機程式版本清單。
                if (!Application.LoadApplicationList(ApplicationListFileName, out Applications))
                {
                    //從檔案取得本機程式版本清單發生錯誤。
                    ReturnResult = false;
                    return ReturnResult;
                }


                //2. 從伺服器取得程式最新版本清單。
                AutoUpdateWebService.AutoUpdateService WebServiceAutoUpdate = new AutoUpdateService();
                DataTable LatestVersionTable = new DataTable();
                LatestVersionTable = WebServiceAutoUpdate.GetApplicationVersion();
                if (LatestVersionTable == null)
                {
                    //從伺服器取得程式版本清單錯誤。                    
                    ReturnResult = false;
                    return ReturnResult;
                }
                if (LatestVersionTable.Rows.Count == 0)
                {
                    //取無最新版本清單。
                    ReturnResult = false;
                    return ReturnResult;
                }


                //3. 迴圈(檢查版本)         
                ApplicationUpdateProcesses = new Dictionary<Guid, Thread>();
                int SleepSeconds = 3;

                int MaxInstallRetry = 3;
                int.TryParse(ConfigurationManager.AppSettings["InstallRetry"], out MaxInstallRetry);
                foreach (Guid LocalApplicationID in Applications.Keys)
                {
                    LatestVersionTable.PrimaryKey = new DataColumn[] { LatestVersionTable.Columns["ApplicationID"] };
                    DataRow LatestVersionRow = LatestVersionTable.Rows.Find(LocalApplicationID);

                    //3.1. 比對清單檢查是否有新版本。
                    bool HasNewVersion = false;

                    Application LocalApplication;
                    Applications.TryGetValue(LocalApplicationID, out LocalApplication);

                    if (!CheckNewVersion(LocalApplication, LatestVersionRow, out HasNewVersion))
                    {
                        //比對時發生錯誤
                        //ReturnResult = false;
                        //return ReturnResult;
                        continue;
                    }

                    //3.1.1 有新版本
                    if (HasNewVersion)
                    {
                        string InstallRetryLogFile = string.Format("{0}\\Version\\{1}.err", Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), LocalApplicationID);
                        if (File.Exists(InstallRetryLogFile))
                        {
                            // MajorVersion,MinorVersion,RetryCount
                            string InstallRetryLog = string.Empty;
                            try
                            {
                                InstallRetryLog = File.ReadAllText(InstallRetryLogFile);
                            }
                            catch (Exception eXc)
                            {
                                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "讀取 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "讀取 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);

                                continue;
                            }
                            string[] LogItems = InstallRetryLog.Split(',');

                            int LogMajroVersion = 0, LogMinorVersion = 0, RetryCount = 0;
                            bool ParseLogItem = true;
                            if (LogItems.Length < 3)
                            {
                                Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 時發生錯誤。", InstallRetryLogFile);
                                Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 時發生錯誤。", InstallRetryLogFile);
                                ParseLogItem = false;
                            }
                            if (ParseLogItem)
                            {
                                if (!int.TryParse(LogItems[0], out LogMajroVersion))
                                {
                                    Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 MajorVersion 時發生錯誤。", InstallRetryLogFile);
                                    Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 MajorVersion 時發生錯誤。", InstallRetryLogFile);

                                    ParseLogItem = false;
                                }
                                if (!int.TryParse(LogItems[1], out LogMinorVersion))
                                {
                                    Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 MinorVersion 時發生錯誤。", InstallRetryLogFile);
                                    Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 MinorVersion 時發生錯誤。", InstallRetryLogFile);

                                    ParseLogItem = false;
                                }
                                if (!int.TryParse(LogItems[2], out RetryCount))
                                {
                                    Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 RetryCount 時發生錯誤。", InstallRetryLogFile);
                                    Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryFail, "解析 \"{0}\" 的 RetryCount 時發生錯誤。", InstallRetryLogFile);

                                    ParseLogItem = false;
                                }
                            }
                            if (!ParseLogItem)
                            {
                                try
                                {
                                    File.Delete(InstallRetryLogFile);
                                }
                                catch (Exception eXc)
                                {
                                    Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                                    Log.LogException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                                }

                                continue;
                            }

                            int MajorVersion = (int)(short)LatestVersionRow["MajorVersion"];
                            int MinorVersion = (int)(short)LatestVersionRow["MinorVersion"];

                            if (MajorVersion == LogMajroVersion && MinorVersion == LogMinorVersion)
                            {
                                // 版本號相同。
                                if (RetryCount > MaxInstallRetry)
                                {
                                    // 超過重試次數
                                    Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_InstallRetryOverMax, "重試安裝次數已超過最大重試次數 {0}。", MaxInstallRetry);
                                    Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_InstallRetryOverMax, "重試安裝次數已超過最大重試次數 {0}。", MaxInstallRetry);

                                    continue;
                                }
                            }
                            else if ((MajorVersion < LogMajroVersion) ||
                                (MajorVersion == LogMajroVersion) && (MinorVersion < LogMinorVersion))
                            {
                                // 版本號小於，不合理。
                                Log.LogError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryVersionFail, "檢查重試安裝次數的版本比對錯誤：{0}.{1} 小於紀錄的 {2}.{3}。", MajorVersion, MinorVersion, LogMajroVersion, LogMinorVersion);
                                Log.TraceError(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryVersionFail, "檢查重試安裝次數的版本比對錯誤：{0}.{1} 小於紀錄的 {2}.{3}。", MajorVersion, MinorVersion, LogMajroVersion, LogMinorVersion);

                                continue;
                            }

                            // 正常程序，請繼續。
                            try
                            {
                                File.Delete(InstallRetryLogFile);
                            }
                            catch (Exception eXc)
                            {
                                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateProcess_CheckRetryException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                            }
                        }

                        // 3.1.1. 有新版本，建立新Process並執行更新程序。
                        ApplicationUpdateProcess ProcessApplicationUpdate = new ApplicationUpdateProcess();
                        ProcessApplicationUpdate.LatestVersionHash = LatestVersionRow["VersionHash"].ToString();
                        ProcessApplicationUpdate.ApplicationEntity = LocalApplication;
                        //起新的Thread
                        #region New Thread

                        ////執行更新程序
                        Thread NewThread = new Thread(new ThreadStart(ProcessApplicationUpdate.UpdateApplication));

                        ApplicationUpdateProcesses.Add(LocalApplicationID, NewThread);
                        NewThread.Start();
                        #endregion

                    }
                    else
                    {
                        // 3.1.2. 無新版本，則繼續下一個迴圈。
                        string ComputerNetbiosName = Environment.MachineName;
                        string ApplicationID = LocalApplication.ID.ToString("D");
                        string ApplicationName = LocalApplication.Name;
                        short MajorVersion = LocalApplication.MajorVersion;
                        short MinorVersion = LocalApplication.MinorVersion;
                        WebServiceAutoUpdate.ReportLatestVersion(ComputerNetbiosName, LocalApplicationID.ToString(), ApplicationName, MajorVersion, MinorVersion);
                        continue;
                    }

                }

                //4. 等待所有更新Process結束並結束程式。
                #region Wait Thread Dead
                while (ApplicationUpdateProcesses.Count > 0)
                {
                    #region Search Dead Thread
                    List<Guid> DeadHandles = new List<Guid>();
                    foreach (Guid ApplicationID in ApplicationUpdateProcesses.Keys)
                    {
                        Thread ProcessHandle = ApplicationUpdateProcesses[ApplicationID];
                        if (!ProcessHandle.IsAlive)
                        {
                            DeadHandles.Add(ApplicationID);
                        }
                    }

                    foreach (Guid ApplicationID in DeadHandles)
                    {
                        ApplicationUpdateProcesses.Remove(ApplicationID);
                    }
                    #endregion

                    if (DeadHandles.Count > 0)
                    {
                        Application.SaveApplicationList(ApplicationListFileName, Applications);
                    }

                    if (ApplicationUpdateProcesses.Count > 0)
                    {
                        Thread.Sleep(SleepSeconds * 1000);
                    }
                }

                #endregion

                ApplicationUpdateProcesses = null;

                //5. 寫入應用程式清單錯誤並結束程式。
                if (!Application.SaveApplicationList(ApplicationListFileName, Applications))
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpateProcess_AutoUpdateException, e, "自動更新作業時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.AutoUpateProcess_AutoUpdateException, e, "自動更新作業時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region CheckNewVersion
        /// <summary>
        /// 比對清單檢查是否有新版本。
        /// </summary>
        /// <param name="LocalApplication">本機應用程式。</param>
        /// <param name="LatestApplicationRow">最新版應用程式。</param>
        /// <param name="HasNewVersion">是否有新版本。</param>
        /// <returns>是否成功。</returns>
        private bool CheckNewVersion(Application LocalApplication, DataRow LatestApplicationRow, out bool HasNewVersion)
        {
            bool ReturnResult = false;
            HasNewVersion = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                //3.1. 比對清單檢查是否有新版本。
                if (LocalApplication.VersionHash == LatestApplicationRow["VersionHash"].ToString())
                {
                    HasNewVersion = false;
                }
                else
                {
                    HasNewVersion = true;
                }

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpateProcess_CheckNewVersionException, e, "比對清單檢查是否有新版本時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.AutoUpateProcess_CheckNewVersionException, e, "比對清單檢查是否有新版本時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion
    }
}
