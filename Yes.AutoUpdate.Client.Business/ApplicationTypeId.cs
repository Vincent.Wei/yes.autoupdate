﻿using System;
using System.Xml.Serialization;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 應用程式類別代碼。
    /// </summary>
    public enum ApplicationTypeId
    {
        /// <summary>
        /// None
        /// </summary>
        [XmlEnum(Name = "None")]
        None = 0,

        /// <summary>
        /// 常駐程式(執行後不會結束,如服務, Daemon)
        /// </summary>
        [XmlEnum(Name = "Resident")]
        Resident = 1,
        /// <summary>
        /// 一般程式(執行後會結束)
        /// </summary>
        [XmlEnum(Name = "General")]
        General = 2,
    }
}
