﻿using System;
using System.Threading;
using SharpBits.Base;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// BITS Helper class。
    /// </summary>
    internal class BITSHelper : Base
    {
        #region Stop
        private bool _Stop = false;
        /// <summary>
        /// 停止等待，馬上結束。
        /// </summary>
        public void Stop()
        {
            _Stop = true;
        }
        #endregion

        #region MaxDownloadDays (3)
        private uint _MaxDownloadDays = 3;
        /// <summary>
        /// 取得或設定最大下載日數，超過此日數，則將提高下載作業優先權。
        /// </summary>
        public uint MaxDownloadDays
        {
            get
            {
                return _MaxDownloadDays;
            }
            set
            {
                _MaxDownloadDays = value;
            }
        }
        #endregion

        #region DownloadRetryDelaySeconds (600)
        private uint _DownloadRetryDelaySeconds = 600;
        /// <summary>
        /// 取得或設定下載錯誤重試之間隔秒數。
        /// </summary>
        public uint DownloadRetryDelaySeconds
        {
            get
            {
                return 600;
            }
            set
            {
                _DownloadRetryDelaySeconds = value;
            }
        }
        #endregion

        #region QueryStatusIntervalSeconds (60)
        private int _QueryStatusIntervalSeconds = 60;
        /// <summary>
        /// 取得或設定查問 BITS 狀態間隔秒數。
        /// </summary>
        public int QueryStatusIntervalSeconds
        {
            get
            {
                return _QueryStatusIntervalSeconds;
            }
            set
            {
                QueryStatusIntervalSeconds = value;
            }
        }
        #endregion

        #region MaxQueryStatusTimes (3)
        private int _MaxQueryStatusTimes = 3;
        /// <summary>
        /// 取得或設定查問 BITS 狀態次數。
        /// </summary>
        public int MaxQueryStatusTimes
        {
            get
            {
                return _MaxQueryStatusTimes;
            }
            set
            {
                _MaxQueryStatusTimes = value;
            }
        }
        #endregion


        #region Constructor
        #region BITSHelper()
        /// <summary>
        /// 建構子。
        /// </summary>
        public BITSHelper()
            : base()
        {
        }
        #endregion
        #region BITSHelper(int MaxQueryStatusTimes, int QueryStatusIntervalSeconds, uint MaxDownloadDays, uint DownloadRetryDelaySeconds)
        /// <summary>
        /// 建構子。
        /// </summary>
        /// <param name="MaxQueryStatusTimes">查問 BITS 狀態次數。</param>
        /// <param name="QueryStatusIntervalSeconds">查問 BITS 狀態間隔秒數。</param>
        /// <param name="MaxDownloadDays">最大下載日數，超過此日數，則將提高下載作業優先權。</param>
        /// <param name="DownloadRetryDelaySeconds">下載錯誤重試之間隔秒數。</param>
        public BITSHelper(int MaxQueryStatusTimes, int QueryStatusIntervalSeconds, uint MaxDownloadDays, uint DownloadRetryDelaySeconds)
        {
            _MaxQueryStatusTimes = MaxQueryStatusTimes;
            _QueryStatusIntervalSeconds = QueryStatusIntervalSeconds;
            _MaxDownloadDays = MaxDownloadDays;
            _DownloadRetryDelaySeconds = DownloadRetryDelaySeconds;
        }
        #endregion
        #endregion


        #region BITSDownload
        /// <summary>
        /// BITS Download。
        /// </summary>
        /// <param name="UniqueJobName">識別名稱，上限256字元。</param>
        /// <param name="DownloadFromUrl">下載網址。</param>
        /// <param name="DownloadToFilePath">下載後完整檔名，路徑必須已存在。</param>
        /// <returns>成功或失敗。</returns>
        public bool BITSDownload(string UniqueJobName, string DownloadFromUrl, string DownloadToFilePath)
        {
            bool ReturnResult = false;

            BitsManager Manager = null;

            try
            {
                Log.TraceMethodEntry(LogSourceName, "UniqueJobName: \"{0}\", DownloadFromUrl: \"{1}\", DownloadToFilePath: \"{2}\"", UniqueJobName, DownloadFromUrl, DownloadToFilePath);

                Manager = new BitsManager();
                Manager.EnumJobs(JobOwner.CurrentUser);

                bool IsNewJob = true;
                foreach (BitsJob Job in Manager.Jobs.Values)
                {
                    if (string.Compare(Job.DisplayName, UniqueJobName, true) != 0)
                    {
                        continue;
                    }

                    IsNewJob = false;
                    break;
                }

                if (IsNewJob)
                {
                    BitsJob NewJob = Manager.CreateJob(UniqueJobName, JobType.Download);
                    NewJob.MinimumRetryDelay = DownloadRetryDelaySeconds;
                    NewJob.NoProgressTimeout = MaxDownloadDays * 24 * 60 * 60;
                    NewJob.AddFile(DownloadFromUrl, DownloadToFilePath);
                    NewJob.Resume();
                }

                int QueryStatusCount = 0;
                while (!_Stop)
                {
                    Manager.EnumJobs(JobOwner.CurrentUser);

                    BitsJob CurrentDownloadJob = null;
                    foreach (BitsJob Job in Manager.Jobs.Values)
                    {
                        if (string.Compare(Job.DisplayName, UniqueJobName, true) != 0)
                        {
                            continue;
                        }

                        CurrentDownloadJob = Job;
                        break;
                    }

                    if (CurrentDownloadJob == null)
                    {
                        Log.TraceError(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadJobNotFound, "BITS Download 找不到 Job \"{0}\"。", UniqueJobName);
                        //Log.LogError(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadJobNotFound, "BITS Download 找不到 Job \"{0}\"。", UniqueJobName);

                        ReturnResult = false;
                        return ReturnResult;
                    }

                    switch (CurrentDownloadJob.State)
                    {
                        case JobState.Suspended:
                            CurrentDownloadJob.Resume();
                            break;

                        case JobState.Error:
                            Log.TraceError(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadJobError, "BITS Download Job \"{0}\" ErrorCode: {1} (\"{2}\")。", UniqueJobName, CurrentDownloadJob.Error == null ? string.Empty : CurrentDownloadJob.Error.ErrorCode.ToString(), CurrentDownloadJob.Error == null ? string.Empty : CurrentDownloadJob.Error.Description);
                            //Log.LogError(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadJobError, "BITS Download Job \"{0}\" ErrorCode: {1} (\"{2}\")。", UniqueJobName, CurrentDownloadJob.Error == null ? string.Empty : CurrentDownloadJob.Error.ErrorCode.ToString(), CurrentDownloadJob.Error == null ? string.Empty : CurrentDownloadJob.Error.Description);
                            CurrentDownloadJob.Cancel();
                            break;

                        case JobState.Transferred:
                            CurrentDownloadJob.Complete();
                            break;

                        case JobState.Queued:
                        case JobState.Connecting:
                        case JobState.Transferring:
                        case JobState.TransientError:
                        case JobState.Acknowledged:
                        case JobState.Canceled:
                        default:
                            break;
                    }

                    if (CurrentDownloadJob.State == JobState.Canceled)
                    {
                        ReturnResult = false;
                        break;
                    }
                    if (CurrentDownloadJob.State == JobState.Acknowledged)
                    {
                        ReturnResult = true;
                        break;
                    }

                    if (CurrentDownloadJob.JobTimes.CreationTime.AddDays(MaxDownloadDays) < DateTime.Now)
                    {
                        CurrentDownloadJob.Priority = JobPriority.High;
                    }

                    QueryStatusCount++;
                    if (QueryStatusCount > MaxQueryStatusTimes)
                    {
                        break;
                    }

                    Thread.Sleep(QueryStatusIntervalSeconds * 1000);
                }

                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadException, e, "BITS Download Job \"{0}\" 從 \"{1}\" 到 \"{2}\" 時發生例外錯誤。", UniqueJobName, DownloadFromUrl, DownloadToFilePath);
                //Log.LogException(LogSourceName, (int)MessageId.BITSHelper_BITSDownloadException, e, "BITS Download Job \"{0}\" 從 \"{1}\" 到 \"{2}\" 時發生例外錯誤。", UniqueJobName, DownloadFromUrl, DownloadToFilePath);

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                #region Manager.Dispose
                if (Manager != null)
                {
                    try
                    {
                        Manager.Dispose();
                    }
                    catch (Exception)
                    {
                    }
                    Manager = null;
                }
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion
    }
}
