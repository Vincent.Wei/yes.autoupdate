﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Xml.Serialization;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 基礎企業類別。
    /// </summary>
    [System.Serializable()]
    public abstract class Base
    {
        #region LogSourceName = "Yes.AutoUpdate.Client.Business"
        /// <summary>
        /// 追蹤來源名稱。
        /// </summary>
        public const string LogSourceName = "Yes.AutoUpdate.Client.Business";
        #endregion

        #region Serializable

        #region XmlSerialize
        /// <summary>
        /// 將企業類別物件序列化為 XML 字串。
        /// </summary>
        /// <returns>表示此企業類別物件的 XML 字串。</returns>
        public string XmlSerialize()
        {
            XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(GetType());

            MemoryStream XmlStream = new System.IO.MemoryStream();

            Serializer.Serialize(XmlStream, this);

            return Encoding.UTF8.GetString(XmlStream.ToArray());
        }
        #endregion

        #region XmlDeserialize
        /// <summary>
        /// 將 XML 字串反序列化為企業類別物件。
        /// </summary>
        /// <param name="XMLString">代表企業類別物件的 XML 字串。</param>
        /// <param name="EntityType">企業類別物件的型別。</param>
        /// <returns>企業類別物件。</returns>
        public static Base XmlDeserialize(string XMLString, System.Type EntityType)
        {
            XmlSerializer Deserializer = new System.Xml.Serialization.XmlSerializer(EntityType);

            MemoryStream XmlStream = new System.IO.MemoryStream();

            XmlStream.Write(Encoding.UTF8.GetBytes(XMLString), 0, Encoding.UTF8.GetByteCount(XMLString));
            XmlStream.Seek(0, SeekOrigin.Begin);

            return (Base)Deserializer.Deserialize(XmlStream);
        }
        #endregion

        #region BinarySerialize
        /// <summary>
        /// 將企業類別物件序列化為二進位格式。
        /// </summary>
        /// <returns>表示此企業類別物件二進位格式的位元陣列。</returns>
        public byte[] BinarySerialize()
        {
            BinaryFormatter Serializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            MemoryStream BinaryStream = new System.IO.MemoryStream();

            Serializer.Serialize(BinaryStream, this);

            return BinaryStream.ToArray();
        }
        #endregion

        #region BinaryDeserialize
        /// <summary>
        /// 將二進位位元陣列反序列化為企業類別物件。
        /// </summary>
        /// <param name="BinaryBytes">代表企業類別物件的二進位位元陣列。</param>
        /// <returns>企業類別物件。</returns>
        public static Base BinaryDeserialize(byte[] BinaryBytes)
        {
            BinaryFormatter Deserializer = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            MemoryStream BinaryStream = new System.IO.MemoryStream();

            BinaryStream.Write(BinaryBytes, 0, BinaryBytes.Length);
            BinaryStream.Seek(0, SeekOrigin.Begin);

            return (Base)Deserializer.Deserialize(BinaryStream);
        }
        #endregion

        #endregion


        #region GetRealPath
        /// <summary>
        /// 轉換系統環境參數，以及相對路徑，取得真實且完整的檔案系統路徑。
        /// </summary>
        /// <param name="OriginalPath">原始路徑。</param>
        /// <returns>真實且完整的檔案系統路徑。</returns>
        public static string GetRealPath(string OriginalPath)
        {
            string ResultPath = string.Empty;

            try
            {
                Log.TraceMethodEntry(LogSourceName, "OriginalPath: \"{0}\"", OriginalPath);

                if (OriginalPath.IndexOf("%") >= 0)
                {
                    string TempPath = OriginalPath;
                    while (TempPath.IndexOf("%") >= 0)
                    {
                        string EnvironmentVariable = string.Empty;
                        int FirstIndex = TempPath.IndexOf("%");
                        int SecondIndex = TempPath.IndexOf("%", FirstIndex + 1);
                        if (SecondIndex > FirstIndex)
                        {
                            EnvironmentVariable = TempPath.Substring(FirstIndex, SecondIndex - FirstIndex + 1);
                        }
                        TempPath = TempPath.Replace(EnvironmentVariable, Environment.GetEnvironmentVariable(EnvironmentVariable.Replace("%", "")));
                    }
                    ResultPath = TempPath;
                }
                else
                {
                    ResultPath = OriginalPath;
                }

                if (ResultPath.StartsWith(".\\")
                    || ResultPath.StartsWith("..\\")
                    || (ResultPath.StartsWith("\\") && (!ResultPath.StartsWith("\\\\")))
                    || (ResultPath.IndexOf(":") < 0 && (!ResultPath.StartsWith("\\\\"))))
                {
                    string CurrentPath = Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);
                    if (!CurrentPath.EndsWith("\\") || !CurrentPath.EndsWith("/"))
                    {
                        CurrentPath += "\\";
                    }

                    if (ResultPath.StartsWith(".\\"))
                    {
                        ResultPath = CurrentPath + ResultPath.Substring(2);
                    }
                    else if (ResultPath.StartsWith("..\\"))
                    {
                        ResultPath = (new DirectoryInfo(CurrentPath).Parent.FullName) + ResultPath.Substring(3);
                    }
                    else if (ResultPath.StartsWith("\\"))
                    {
                        ResultPath = CurrentPath + ResultPath.Substring(1);
                    }
                    else
                    {
                        ResultPath = CurrentPath + ResultPath;
                    }
                }

                return ResultPath;
            }
            catch (Exception e)
            {
                ResultPath = string.Empty;
                throw e;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ResultPath);
            }
        }
        #endregion
    }
}
