﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Text;
using Ionic.Zip;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 應用程式。
    /// </summary>
    public class Application : Base
    {
        #region Property

        #region ID
        /// <summary>
        /// 應用程式ID。
        /// </summary>
        private Guid _ID = Guid.Empty;
        private string _IDString = string.Empty;
        /// <summary>
        /// 取得或設定應用程式ID。
        /// </summary>
        /// <value>應用程式ID。</value>
        public Guid ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
                _IDString = value.ToString("D");
            }
        }
        #endregion

        #region Name
        /// <summary>
        /// 應用程式名稱。
        /// </summary>
        private string _Name = string.Empty;
        /// <summary>
        /// 取得或設定應用程式名稱。
        /// </summary>
        /// <value>應用程式名稱。</value>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        #endregion

        #region Description
        /// <summary>
        /// 應用程式備註。
        /// </summary>
        private string _Description = string.Empty;
        /// <summary>
        /// 取得或設定應用程式備註。
        /// </summary>
        /// <value>應用程式備註。</value>
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }
        #endregion

        #region VersionHash
        /// <summary>
        /// 應用程式版本雜湊值。
        /// </summary>
        private string _VersionHash = string.Empty;
        /// <summary>
        /// 取得或設定應用程式版本雜湊值。
        /// </summary>
        /// <value>應用程式版本雜湊值。</value>
        public string VersionHash
        {
            get
            {
                return _VersionHash;
            }
            set
            {
                _VersionHash = value;
            }
        }
        #endregion

        #region MajorVersion
        /// <summary>
        /// 應用程式主要版本。
        /// </summary>
        private short _MajorVersion = short.MinValue;
        /// <summary>
        /// 取得或設定應用程式主要版本。
        /// </summary>
        /// <value>應用程式主要版本。</value>
        public short MajorVersion
        {
            get
            {
                return _MajorVersion;
            }
            set
            {
                _MajorVersion = value;
            }
        }
        #endregion

        #region MinorVersion
        /// <summary>
        /// 應用程式次要版本。
        /// </summary>
        private short _MinorVersion = short.MinValue;
        /// <summary>
        /// 取得或設定應用程式次要版本。
        /// </summary>
        /// <value>應用程式次要版本。</value>
        public short MinorVersion
        {
            get
            {
                return _MinorVersion;
            }
            set
            {
                _MinorVersion = value;
            }
        }
        #endregion

        #region LocationPath
        /// <summary>
        /// 應用程式所在路徑。
        /// </summary>
        private string _LocationPath = string.Empty;
        /// <summary>
        /// 取得或設定應用程式所在路徑。
        /// </summary>
        /// <value>應用程式所在路徑。</value>
        public string LocationPath
        {
            get
            {
                return _LocationPath;
            }
            set
            {
                _LocationPath = value;
            }
        }
        #endregion

        #region ApplicationType
        /// <summary>
        /// 應用程式類型。
        /// </summary>
        private ApplicationTypeId _ApplicationType = ApplicationTypeId.None;
        /// <summary>
        /// 取得或設定應用程式類型。
        /// </summary>
        /// <value>應用程式類型。</value>
        public ApplicationTypeId ApplicationType
        {
            get
            {
                return _ApplicationType;
            }
            set
            {
                _ApplicationType = value;
            }
        }
        #endregion

        #region BackupExcludeSubfolders
        /// <summary>
        /// 應用程式備份例外資料夾。
        /// </summary>
        private string[] _BackupExcludeSubfolders = null;
        /// <summary>
        /// 取得或設定應用程式備份例外資料夾。
        /// </summary>
        /// <value>應用程式備份例外資料夾。</value>
        public string[] BackupExcludeSubfolders
        {
            get
            {
                return _BackupExcludeSubfolders;
            }
            set
            {
                _BackupExcludeSubfolders = value;
            }
        }
        #endregion

        #region IsNeedReportVersion
        /// <summary>
        /// 是否必須由應用程式本身定期回報版本。
        /// </summary>
        private bool _IsNeedReportVersion = false;
        /// <summary>
        /// 取得或設定是否必須由應用程式本身定期回報版本。
        /// </summary>
        /// <value>是否必須由應用程式本身定期回報版本。</value>
        public bool IsNeedReportVersion
        {
            get
            {
                return _IsNeedReportVersion;
            }
            set
            {
                _IsNeedReportVersion = value;
            }
        }
        #endregion

        #endregion

        /// <summary>
        /// 版本檔案格式，"目錄\檔名.xml"。
        /// </summary>
        private const string VersionFileNameFormat = "{0}\\{1}.xml";

        #region LoadApplicationList
        /// <summary>
        /// 從檔案讀取應用程式清單。
        /// 1. 從指定檔案讀取應用程式清單。
        ///     檔案內容格式為 Guid,Hash
        /// 2. 將檔案內容分析存入應用程式列表。
        /// </summary>
        /// <param name="ApplicationListFileName">應用程式清單檔案名稱。</param>
        /// <param name="Applications">應用程式清單。</param>
        /// <returns>是否成功。</returns>
        public static bool LoadApplicationList(string ApplicationListFileName, out Dictionary<Guid, Application> Applications)
        {
            bool ReturnResult = false;
            Applications = null;
            try
            {
                Log.TraceMethodEntry(LogSourceName, "ApplicationListFileName: \"{0}\"", ApplicationListFileName);

                //檢查檔案是否存在
                if (!File.Exists(ApplicationListFileName))
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                //讀取檔案內容
                string[] ApplicationInformations = File.ReadAllLines(ApplicationListFileName, Encoding.UTF8);

                Applications = new Dictionary<Guid, Application>();

                #region 將檔案內容分析存入應用程式列表
                foreach (string ApplicationInformation in ApplicationInformations)
                {
                    Application LocalApplication = new Application();
                    int SeparatorAccess = ApplicationInformation.IndexOf(',');
                    Guid ID = new Guid(ApplicationInformation.Substring(0, SeparatorAccess));
                    string Hash = ApplicationInformation.Substring(SeparatorAccess + 1);

                    LocalApplication.ID = ID;
                    LocalApplication.VersionHash = Hash;

                    Applications.Add(ID, LocalApplication);
                }
                #endregion

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_LoadApplicationListException, e, "從檔案讀取應用程式清單時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_LoadApplicationListException, e, "從檔案讀取應用程式清單時發生例外錯誤。");
                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region SaveApplicationList
        /// <summary>
        /// 寫入應用程式清單檔案。
        /// </summary>
        /// <param name="ApplicationListFileName">應用程式清單檔案名稱。</param>
        /// <param name="Applications">應用程式清單。</param>
        /// <returns>是否成功。</returns>
        public static bool SaveApplicationList(string ApplicationListFileName, Dictionary<Guid, Application> Applications)
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, "ApplicationListFileName: \"{0}\"; ApplicationsCount: \"{1}\"", ApplicationListFileName, Applications.Count);

                string FileContent = string.Empty;
                string ContentLineFormat = "{0},{1}\r\n";

                #region 取得清單內容
                foreach (Application LocalApplication in Applications.Values)
                {
                    FileContent += string.Format(ContentLineFormat, LocalApplication.ID.ToString("D"), LocalApplication.VersionHash);
                }
                #endregion

                //將內容寫入檔案
                File.WriteAllText(ApplicationListFileName, FileContent, Encoding.UTF8);

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_SaveApplicationListException, e, "寫入應用程式清單檔案時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_SaveApplicationListException, e, "寫入應用程式清單檔案時發生例外錯誤。");
                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region LoadApplication
        /// <summary>
        /// 從檔案取得應用程式資訊。
        /// 1.從檔案讀取Xml字串。
        /// 2.將Xml字串反序列化。
        /// </summary>
        /// <returns>是否成功。</returns>
        public bool LoadApplication()
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                #region 從config取得路徑
                string VersionFolder = GetRealPath(ConfigurationManager.AppSettings["VersionFolder"]);
                string VersionFilePath = string.Format(VersionFileNameFormat, VersionFolder, _IDString);
                #endregion

                if (!File.Exists(VersionFilePath))
                {
                    //檔案不存在。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_LoadApplicationFileNotExistsError, "XML檔案不存在，檔案: \"{0}\"。", VersionFilePath);
                    Log.LogError(LogSourceName, (int)MessageId.Application_LoadApplicationFileNotExistsError, "XML檔案不存在，檔案: \"{0}\"。", VersionFilePath);

                    ReturnResult = false;
                    return ReturnResult;
                }

                #region XML反序列化
                string VersionXML = File.ReadAllText(VersionFilePath);
                Application TempApplication = null;
                try
                {
                    TempApplication = (Application)Base.XmlDeserialize(VersionXML, typeof(Application));
                }
                catch (Exception e)
                {
                    //解析XML失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", VersionFilePath, e);
                    Log.LogError(LogSourceName, (int)MessageId.Application_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", VersionFilePath, e);
                    ReturnResult = false;
                    return ReturnResult;
                }

                ID = TempApplication.ID;
                Name = TempApplication.Name;
                Description = TempApplication.Description;
                //VersionHash = TempApplication.VersionHash;
                MajorVersion = TempApplication.MajorVersion;
                MinorVersion = TempApplication.MinorVersion;
                LocationPath = TempApplication.LocationPath;
                ApplicationType = TempApplication.ApplicationType;
                BackupExcludeSubfolders = TempApplication.BackupExcludeSubfolders;
                #endregion

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_LoadApplicationException, e, "從檔案取得應用程式資訊時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_LoadApplicationException, e, "從檔案取得應用程式資訊時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region DownloadNewVersion
        /// <summary>
        /// 下載應用程式最新版本並解壓縮至指定目錄。
        /// 1. 下載至 /Download/{ApplicationID}/
        /// 2. 下載完成解壓縮至 /Update/{ApplicationID}/
        /// </summary>
        /// <param name="DownloadURL">新版本下載位置。</param>
        /// <returns>是否成功。</returns>
        public bool DownloadNewVersion(string DownloadURL)
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, "DownloadURL: \"{0}\"", DownloadURL);

                // Ignore SSL certificate errors
                ServicePointManager.ServerCertificateValidationCallback =
                   new RemoteCertificateValidationCallback(
                       delegate
                       { return true; }
                   );

                #region 從config取得路徑
                string DownloadFolder = GetRealPath(ConfigurationManager.AppSettings["DownloadFolder"]);
                string UpdateFolder = GetRealPath(ConfigurationManager.AppSettings["UpdateFolder"]);

                if (!DownloadFolder.EndsWith("\\"))
                {
                    DownloadFolder += "\\";
                }
                if (!UpdateFolder.EndsWith("\\"))
                {
                    UpdateFolder += "\\";
                }

                string DownloadLocalFileName = string.Format("{0}\\UpdateZip.zip", _IDString);

                if (!Directory.Exists(DownloadFolder + _IDString))
                {
                    Directory.CreateDirectory(DownloadFolder + _IDString);
                }
                #endregion

                #region 下載檔案
                //WebClient DownloadClinet = new WebClient();
                //DownloadClinet.DownloadFile(
                //    DownloadURL,
                //    DownloadFolder + DownloadLocalFileName);

                BITSHelper DownloadHelper = new BITSHelper(
                    int.Parse(ConfigurationManager.AppSettings["BITSMaxQueryStatusTimes"]),
                    int.Parse(ConfigurationManager.AppSettings["BITSQueryStatusIntervalSeconds"]),
                    uint.Parse(ConfigurationManager.AppSettings["BITSMaxDownloadDays"]),
                    uint.Parse(ConfigurationManager.AppSettings["BITSDownloadRetryDelaySeconds"]));

                DownloadHelper.BITSDownload(string.Format("{0}.{1:000}.{2:000}", _IDString, MajorVersion, MinorVersion),
                    DownloadURL, string.Format("{0}{1}", DownloadFolder, DownloadLocalFileName));
                #endregion

                //避免下載的zip檔案被鎖住，造成無法存取
                System.Threading.Thread.Sleep(5 * 1000);

                #region 解壓縮
                if (!Unzip(
                    DownloadFolder + DownloadLocalFileName,
                    UpdateFolder + _IDString))
                {
                    //解壓縮失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_DownloadNewVersionError, "下載應用程式最新版本並解壓縮至指定目錄發生錯誤，檔案: \"{0}\"。", DownloadLocalFileName);
                    Log.LogError(LogSourceName, (int)MessageId.Application_DownloadNewVersionError, "下載應用程式最新版本並解壓縮至指定目錄發生錯誤，檔案: \"{0}\"。", DownloadLocalFileName);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                Directory.Delete(DownloadFolder + _IDString, true);

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_DownloadNewVersionException, e, "下載應用程式最新版本並解壓縮至指定目錄時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_DownloadNewVersionException, e, "下載應用程式最新版本並解壓縮至指定目錄時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region UpdateVersion
        /// <summary>
        /// 更新應用程式。
        /// 1. 儲存LocalPath至LocationPathFile。
        /// 2. 執行更新程式 /Update/{ApplicationID}/{PreExecutionFileName}(例："PreUpdate.bat")。
        ///    執行更新程式 /Update/{ApplicationID}/{UpdateExecutionFileName}(例："Update.bat")。
        ///    執行更新程式 /Update/{ApplicationID}/{PostExecutionFileName}(例："PostUpdate.bat")。
        /// 3. 讀取LocationPathFile取得最新路徑。
        /// 4. 由 /Update/{ApplicationID}/{ApplicationID}.xml 更新應用程式更新應用程式版本資訊檔 /Version/{ApplicationID}.xml。
        /// 5. 刪除備份(暫存)檔案。
        /// </summary>
        /// <param name="LatestVersionHash">新版本雜湊值。</param>
        /// <param name="UpdateWaitingMinutes">等待更新程式執行結束最大分鐘數，若為0，則為無限期等待。</param>
        /// <param name="ExitCode">更新程式結束回傳值。</param>
        /// <returns>是否成功。</returns>
        public bool UpdateVersion(string LatestVersionHash, int UpdateWaitingMinutes, out int ExitCode)
        {
            bool ReturnResult = false;
            ExitCode = int.MinValue;

            int UpdateExitCode = int.MinValue;
            int PreUpdateExitCode = int.MinValue;
            int PostUpdateExitCode = int.MinValue;


            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                #region 從config取得路徑
                string VersionFolder = GetRealPath(ConfigurationManager.AppSettings["VersionFolder"]);
                string VersionFile = ConfigurationManager.AppSettings["VersionFile"];

                string DownloadFolder = GetRealPath(ConfigurationManager.AppSettings["DownloadFolder"]);
                string UpdateFolder = GetRealPath(ConfigurationManager.AppSettings["UpdateFolder"]);
                string BackupFolder = GetRealPath(ConfigurationManager.AppSettings["BackupFolder"]);
                string UpdateExecutionFileName = ConfigurationManager.AppSettings["UpdateExecutionFileName"];
                string PreExecutionFileName = ConfigurationManager.AppSettings["PreExecutionFileName"];
                string PostExecutionFileName = ConfigurationManager.AppSettings["PostExecutionFileName"];
                string LocationPathFile = ConfigurationManager.AppSettings["LocationPathFile"];

                if (!VersionFolder.EndsWith("\\"))
                {
                    VersionFolder += "\\";
                }
                if (!DownloadFolder.EndsWith("\\"))
                {
                    DownloadFolder += "\\";
                }
                if (!UpdateFolder.EndsWith("\\"))
                {
                    UpdateFolder += "\\";
                }
                if (!BackupFolder.EndsWith("\\"))
                {
                    BackupFolder += "\\";
                }
                #endregion

                FileInfo ExecutionFileInfo = null;
                System.Diagnostics.Process ExecutionProcess = null;
                UpdateFolder += _IDString + "\\";
                BackupFolder += _IDString + "\\";

                File.WriteAllText(UpdateFolder + LocationPathFile, _LocationPath, Encoding.UTF8);

                #region 執行程式 PreExecutionFileName
                ExecutionFileInfo = new FileInfo(UpdateFolder + PreExecutionFileName);

                ExecutionProcess = new System.Diagnostics.Process();
                ExecutionProcess.StartInfo.UseShellExecute = true;
                ExecutionProcess.StartInfo.FileName = ExecutionFileInfo.FullName;
                ExecutionProcess.StartInfo.WorkingDirectory = ExecutionFileInfo.DirectoryName;
                ExecutionProcess.StartInfo.RedirectStandardOutput = false;
                ExecutionProcess.StartInfo.RedirectStandardError = false;
                ExecutionProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                ExecutionProcess.Start();
                if (UpdateWaitingMinutes > 0)
                {
                    ExecutionProcess.WaitForExit(UpdateWaitingMinutes * 60 * 1000);
                }
                else
                {
                    ExecutionProcess.WaitForExit();
                }
                PreUpdateExitCode = ExecutionProcess.ExitCode;
                ExecutionProcess = null;
                ExecutionFileInfo = null;

                if (PreUpdateExitCode != 0)
                {
                    ExitCode = PreUpdateExitCode;
                    //執行失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PreExecutionFileName, ExitCode);
                    Log.LogError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PreExecutionFileName, ExitCode);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                #region 執行程式 UpdateExecutionFileName
                ExecutionFileInfo = new FileInfo(UpdateFolder + UpdateExecutionFileName);

                ExecutionProcess = new System.Diagnostics.Process();
                ExecutionProcess.StartInfo.UseShellExecute = true;
                ExecutionProcess.StartInfo.FileName = ExecutionFileInfo.FullName;
                ExecutionProcess.StartInfo.WorkingDirectory = ExecutionFileInfo.DirectoryName;
                ExecutionProcess.StartInfo.RedirectStandardOutput = false;
                ExecutionProcess.StartInfo.RedirectStandardError = false;
                ExecutionProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                ExecutionProcess.Start();
                if (UpdateWaitingMinutes > 0)
                {
                    ExecutionProcess.WaitForExit(UpdateWaitingMinutes * 60 * 1000);
                }
                else
                {
                    ExecutionProcess.WaitForExit();
                }
                UpdateExitCode = ExecutionProcess.ExitCode;
                ExecutionProcess = null;
                ExecutionFileInfo = null;

                if (UpdateExitCode != 0)
                {
                    ExitCode = UpdateExitCode;
                    //執行失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + UpdateExecutionFileName, ExitCode);
                    Log.LogError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + UpdateExecutionFileName, ExitCode);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                #region 執行程式 PostExecutionFileName
                ExecutionFileInfo = new FileInfo(UpdateFolder + PostExecutionFileName);

                ExecutionProcess = new System.Diagnostics.Process();
                ExecutionProcess.StartInfo.UseShellExecute = true;
                ExecutionProcess.StartInfo.FileName = ExecutionFileInfo.FullName;
                ExecutionProcess.StartInfo.WorkingDirectory = ExecutionFileInfo.DirectoryName;
                ExecutionProcess.StartInfo.RedirectStandardOutput = false;
                ExecutionProcess.StartInfo.RedirectStandardError = false;
                ExecutionProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                ExecutionProcess.Start();
                if (UpdateWaitingMinutes > 0)
                {
                    ExecutionProcess.WaitForExit(UpdateWaitingMinutes * 60 * 1000);
                }
                else
                {
                    ExecutionProcess.WaitForExit();
                }
                PostUpdateExitCode = ExecutionProcess.ExitCode;
                ExecutionProcess = null;
                ExecutionFileInfo = null;

                if (PostUpdateExitCode != 0)
                {
                    ExitCode = PostUpdateExitCode;
                    //執行失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PostExecutionFileName, ExitCode);
                    Log.LogError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PostExecutionFileName, ExitCode);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion


                #region XML反序列化
                string UpdateXmlFileFolder = UpdateFolder + "\\";

                string UpdateVersionFilePath = string.Format("{0}\\{1}", UpdateXmlFileFolder, VersionFile);
                string VersionXML = File.ReadAllText(UpdateVersionFilePath);
                Application TempApplication = null;
                try
                {
                    TempApplication = (Application)Base.XmlDeserialize(VersionXML, typeof(Application));
                }
                catch (Exception e)
                {
                    //解析XML失敗。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", UpdateVersionFilePath, e);
                    Log.LogError(LogSourceName, (int)MessageId.Application_LoadApplicationXmlDeserializeError, "XML反序列化時發生例外錯誤，檔案: \"{0}\"。錯誤訊息: \"{1}\"。", UpdateVersionFilePath, e);
                    ReturnResult = false;
                    return ReturnResult;
                }

                ID = TempApplication.ID;
                Name = TempApplication.Name;
                Description = TempApplication.Description;
                VersionHash = LatestVersionHash;
                MajorVersion = TempApplication.MajorVersion;
                MinorVersion = TempApplication.MinorVersion;
                ApplicationType = TempApplication.ApplicationType;
                BackupExcludeSubfolders = TempApplication.BackupExcludeSubfolders;

                #endregion

                string TempLocalPath = File.ReadAllText(UpdateFolder + LocationPathFile);
                LocationPath = TempLocalPath.Replace("\r\n", string.Empty);

                string NewVersionXML = this.XmlSerialize();

                string VersionFilePath = string.Format(VersionFileNameFormat, VersionFolder, _IDString);
                File.WriteAllText(VersionFilePath, NewVersionXML);

                Directory.Delete(UpdateFolder, true);
                Directory.Delete(BackupFolder, true);

                ExitCode = 0;

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_UpdateVersionException, e, "更新應用程式時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_UpdateVersionException, e, "更新應用程式時發生例外錯誤。");

                UpdateExitCode = int.MinValue;
                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}, UpdateExitCode: {1}", ReturnResult, UpdateExitCode);
            }
        }
        #endregion

        #region BackupVersion
        /// <summary>
        /// 備份原有應用程式檔案。
        /// 備份至 /Backup/{ApplicationID}/
        /// </summary>
        /// <returns>是否成功。</returns>
        public bool BackupVersion()
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                #region 從config取得路徑
                string BackupFolder = GetRealPath(ConfigurationManager.AppSettings["BackupFolder"]);
                if (!BackupFolder.EndsWith("\\"))
                {
                    BackupFolder += "\\";
                }
                BackupFolder += _IDString + "\\";
                #endregion

                if (System.IO.Directory.Exists(_LocationPath))
                {
                    #region 複製檔案至備份目錄
                    string[] Files = System.IO.Directory.GetFiles(_LocationPath, "*", SearchOption.AllDirectories);

                    bool IsExclude = false;

                    foreach (string File in Files)
                    {
                        IsExclude = false;

                        //原始檔案
                        foreach (string BackupExcludeSubfolder in _BackupExcludeSubfolders)
                        {
                            if (File.Contains(BackupExcludeSubfolder))
                            {
                                //不需複製的目錄。
                                IsExclude = true;
                                break;
                            }
                        }

                        if (IsExclude)
                        {
                            continue;
                        }

                        string CopyFilePath = File.Replace(_LocationPath, BackupFolder);

                        //複製的檔案
                        if (!System.IO.Directory.Exists(Path.GetDirectoryName(CopyFilePath)))
                        {
                            System.IO.Directory.CreateDirectory(Path.GetDirectoryName(CopyFilePath));
                        }

                        System.IO.File.Copy(File, CopyFilePath, true);
                    }
                    #endregion
                }
                else
                {
                    //目錄不存在。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_BackupVersionError, "備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。", _LocationPath);
                    Log.LogError(LogSourceName, (int)MessageId.Application_BackupVersionError, "備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。", _LocationPath);

                    ReturnResult = false;
                    return ReturnResult;
                }

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_BackupVersionException, e, "備份原有應用程式檔案時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_BackupVersionException, e, "備份原有應用程式檔案時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region RollbackVersion
        /// <summary>
        /// 還原已備份之應用程式。
        /// 1. 從 /Backup/{ApplicationID}/ 還原應用程式
        /// 2. 執行更新還原程式 /Update/{ApplicationID}/{PostRollBackFileName}(例："PostRollback.bat")。
        /// </summary>
        /// <returns>是否成功。</returns>
        public bool RollbackVersion()
        {
            bool ReturnResult = false;
            int ExitCode = int.MinValue;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                #region 從config取得路徑
                string PostRollBackFileName = ConfigurationManager.AppSettings["PostRollbackFileName"];
                string UpdateFolder = GetRealPath(ConfigurationManager.AppSettings["UpdateFolder"]);
                string BackupFolder = GetRealPath(ConfigurationManager.AppSettings["BackupFolder"]);
                if (!UpdateFolder.EndsWith("\\"))
                {
                    UpdateFolder += "\\";
                }

                if (!BackupFolder.EndsWith("\\"))
                {
                    BackupFolder += "\\";
                }

                BackupFolder += _IDString + "\\";
                #endregion

                if (System.IO.Directory.Exists(BackupFolder))
                {
                    #region 從備份資料夾複製檔案至安裝路徑
                    string[] Files = System.IO.Directory.GetFiles(BackupFolder, "*", SearchOption.AllDirectories);

                    foreach (string File in Files)
                    {
                        //原始檔案
                        FileInfo FileName = new FileInfo(File);
                        string CopyFilePath = FileName.FullName.Replace(BackupFolder, _LocationPath);

                        //複製的檔案
                        FileInfo CopyFile = new FileInfo(CopyFilePath);

                        if (!CopyFile.Directory.Exists)
                        {
                            System.IO.Directory.CreateDirectory(CopyFile.DirectoryName);
                        }

                        System.IO.File.Copy(FileName.FullName, CopyFile.FullName, true);
                    }
                    #endregion

                    FileInfo ExecutionFileInfo = null;
                    System.Diagnostics.Process ExecutionProcess = null;
                    UpdateFolder += _IDString + "\\";

                    #region 執行程式 UpdateExecutionFileName
                    ExecutionFileInfo = new FileInfo(UpdateFolder + PostRollBackFileName);

                    ExecutionProcess = new System.Diagnostics.Process();
                    ExecutionProcess.StartInfo.UseShellExecute = true;
                    ExecutionProcess.StartInfo.FileName = ExecutionFileInfo.FullName;
                    ExecutionProcess.StartInfo.WorkingDirectory = ExecutionFileInfo.DirectoryName;
                    ExecutionProcess.StartInfo.RedirectStandardOutput = false;
                    ExecutionProcess.StartInfo.RedirectStandardError = false;
                    ExecutionProcess.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    ExecutionProcess.Start();
                    ExecutionProcess.WaitForExit();
                    ExitCode = ExecutionProcess.ExitCode;
                    ExecutionProcess = null;
                    ExecutionFileInfo = null;


                    if (ExitCode != 0)
                    {
                        //執行失敗。
                        Log.TraceError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PostRollBackFileName, ExitCode);
                        Log.LogError(LogSourceName, (int)MessageId.Application_UpdateVersionError, "執行更新程式發生錯誤，檔案: \"{0}\" ExitCode: \"{1}\"。", UpdateFolder + PostRollBackFileName, ExitCode);

                    }
                    #endregion

                }
                else
                {
                    //目錄不存在。
                    Log.TraceError(LogSourceName, (int)MessageId.Application_RollbackVersionError, "備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。", BackupFolder);
                    Log.LogError(LogSourceName, (int)MessageId.Application_RollbackVersionError, "備份原有應用程式檔案發生錯誤，目錄 \"{0}\" 不存在。", BackupFolder);

                }

                Directory.Delete(BackupFolder, true);

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_RollbackVersionException, e, "還原已備份之應用程式時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.Application_RollbackVersionException, e, "還原已備份之應用程式時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region Unzip
        /// <summary>
        /// 解壓縮。
        /// </summary>
        /// <param name="ZipFileFullName">欲解壓縮檔案完整路徑。</param>
        /// <param name="UnZipFolderPath">欲解壓縮至目錄之路徑。</param>
        /// <returns>是否成功。</returns>
        private static bool Unzip(string ZipFileFullName, string UnZipFolderPath)
        {
            bool ReturnResult = false;
            try
            {
                Log.TraceMethodEntry(LogSourceName, "ZipFileFullName: \"{0}\", UnZipFolderPath: \"{1}\"", ZipFileFullName, UnZipFolderPath);

                #region 檢查 Zip 檔是否存在
                if (!File.Exists(ZipFileFullName))
                {
                    Log.TraceError(LogSourceName, (int)MessageId.Application_UnzipError, "解壓縮發生錯誤，檔案 \"{0}\" 不存在。", ZipFileFullName);
                    Log.LogError(LogSourceName, (int)MessageId.Application_UnzipError, "解壓縮發生錯誤，檔案 \"{0}\" 不存在。", ZipFileFullName);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                #region 檢查並建立目標輸出路徑
                if (!UnZipFolderPath.EndsWith("\\"))
                {
                    UnZipFolderPath += "\\";
                }
                DirectoryInfo TargetDirectoryInfo = new DirectoryInfo(UnZipFolderPath);
                if (!TargetDirectoryInfo.Exists)
                {
                    TargetDirectoryInfo.Create();
                    TargetDirectoryInfo.Refresh();

                    if (!TargetDirectoryInfo.Exists)
                    {
                        ReturnResult = false;
                        return ReturnResult;
                    }
                }
                #endregion

                #region 解壓縮
                using (Ionic.Zip.ZipFile zip = ZipFile.Read(ZipFileFullName))
                {
                    foreach (ZipEntry e in zip)
                    {
                        e.Extract(TargetDirectoryInfo.FullName, true);
                    }
                }
                #endregion

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.Application_UnzipException, e, "解壓縮時發生例外錯誤，檔案: \"{0}\"。", ZipFileFullName);
                Log.LogException(LogSourceName, (int)MessageId.Application_UnzipException, e, "解壓縮時發生例外錯誤，檔案: \"{0}\"。", ZipFileFullName);

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion
    }
}
