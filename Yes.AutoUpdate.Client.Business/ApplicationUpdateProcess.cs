﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using System.Net;
using System.Net.Security;
using Yes.AutoUpdate.Client.Business.AutoUpdateWebService;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Client.Business
{
    /// <summary>
    /// 應用程式更新程序類別。
    /// </summary>
    public class ApplicationUpdateProcess : Base
    {
        #region ApplicationEntity
        /// <summary>
        /// 應用程式。
        /// </summary>
        private Application _ApplicationEntity = null;
        /// <summary>
        /// 取得或設定應用程式。
        /// </summary>
        /// <value>應用程式(Application)。</value>
        public Application ApplicationEntity
        {
            get
            {
                return _ApplicationEntity;
            }
            set
            {
                _ApplicationEntity = value;
            }
        }
        #endregion

        #region LatestVersionHash
        /// <summary>
        /// 最新版本Hash值。
        /// </summary>
        private string _LatestVersionHash = string.Empty;
        /// <summary>
        /// 取得或設定最新版本Hash值。
        /// </summary>
        /// <value>最新版本Hash值。</value>
        public string LatestVersionHash
        {
            get
            {
                return _LatestVersionHash;
            }
            set
            {
                _LatestVersionHash = value;
            }
        }
        #endregion

        #region UpdateApplication
        /// <summary>
        /// 更新程序
        /// 1. 從伺服器取得下載檔案路徑位置。 AutoUpdateWebService.AutoUpdateService.GetNewVesion()
        /// 2. 從指定伺服器下載新版本並解壓縮至指定路徑。 Application.DownloadNewVersion()
        /// 3. 載入應用程式版本資訊檔。  Application.LoadApplication()
        /// 4. 備份版本。 Application.BackupVersion()
        /// 5. 更新版本作業。 Application.UpdateVersion()
        /// 6. 判斷(更新版本是否成功)
        ///     6-1. 更新失敗，還原舊版本。 Application.RollbackVersion()
        /// 7. 無論是否更新成功，回報更新版本資訊後結果給Server。 AutoUpdateWebService.AutoUpdateService.ReportUpdateVersionResult()
        /// </summary>
        public void UpdateApplication()
        {
            bool Error = false;
            string ApplicationName = string.Empty;
            short MajorVersion = short.MinValue;
            short MinorVersion = short.MinValue;
            string DownlaodUrl = string.Empty;
            int UpdateWaitingMinutes = int.MinValue;

            int UpdateExitCode = int.MinValue;
            bool UpdateResult = false;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                // Ignore SSL certificate errors
                ServicePointManager.ServerCertificateValidationCallback =
                   new RemoteCertificateValidationCallback(
                       delegate
                       { return true; }
                   );

                AutoUpdateWebService.AutoUpdateService WebServiceAutoUpdate = new AutoUpdateService();

                string ApplicationID = ApplicationEntity.ID.ToString("D");


                //1. 從伺服器取得下載檔案路徑位置。
                string GetNewVersionXML = string.Empty;
                if (!Error)
                {
                    GetNewVersionXML = WebServiceAutoUpdate.GetNewVersion(ApplicationID);
                }

                if (GetNewVersionXML == string.Empty)
                {
                    //取得下載路徑時發生錯誤。
                    Error = true;
                }

                if (!Error)
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    XmlDoc.LoadXml(GetNewVersionXML);

                    XmlNode Root = XmlDoc.DocumentElement;

                    ApplicationName = Root.SelectSingleNode("ApplicationName").InnerText;
                    MajorVersion = Convert.ToInt16(Root.SelectSingleNode("MajorVersion").InnerText);
                    MinorVersion = Convert.ToInt16(Root.SelectSingleNode("MinorVersion").InnerText);
                    DownlaodUrl = Root.SelectSingleNode("DownloadUrl").InnerText;
                    UpdateWaitingMinutes = Convert.ToInt32(Root.SelectSingleNode("UpdateWaitingMinutes").InnerText);
                }

                //2. 從指定伺服器下載新版本並解壓縮至指定路徑。
                if (!ApplicationEntity.DownloadNewVersion(DownlaodUrl) && !Error)
                {
                    //從指定伺服器下載新版本並解壓縮至指定路徑失敗。
                    Error = true;
                }


                //3. 載入應用程式版本資訊檔。
                if (!ApplicationEntity.LoadApplication() && !Error)
                {
                    //載入應用程式本版資訊檔發生錯誤。
                    Error = true;
                }


                //4. 備份版本。
                if (!ApplicationEntity.BackupVersion() && !Error)
                {
                    //備份版本時發生錯誤。
                    Error = true;
                }


                //5. 更新版本作業。
                //6. 判斷(更新版本是否成功)。
                if (!Error)
                {
                    UpdateResult = ApplicationEntity.UpdateVersion(LatestVersionHash, UpdateWaitingMinutes, out UpdateExitCode);

                    string InstallRetryLogFile = string.Format("{0}\\Version\\{1}.err", Path.GetDirectoryName(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName), ApplicationEntity.ID);
                    if (!UpdateResult)
                    {
                        //6-1. 更新失敗，還原舊版本。
                        if (!ApplicationEntity.RollbackVersion())
                        {
                            //還原舊版本失敗。
                            Error = true;
                        }

                        int RetryCount = 0;
                        if (File.Exists(InstallRetryLogFile))
                        {
                            // MajorVersion,MinorVersion,RetryCount
                            string InstallRetryLog = string.Empty;
                            try
                            {
                                InstallRetryLog = File.ReadAllText(InstallRetryLogFile);
                            }
                            catch (Exception eXc)
                            {
                                Log.TraceException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_ReadRetryLogException, eXc, "讀取 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                                Log.LogException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_ReadRetryLogException, eXc, "讀取 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                            }
                            string[] LogItems = InstallRetryLog.Split(',');

                            int LogMajroVersion, LogMinorVersion;
                            if (LogItems.Length < 3)
                            {
                                Log.LogError(LogSourceName, (int)MessageId.ApplicationUpdateProcess_ParseRetryLogException, "解析 \"{0}\" 時發生錯誤。", InstallRetryLogFile);
                                Log.TraceError(LogSourceName, (int)MessageId.ApplicationUpdateProcess_ParseRetryLogException, "解析 \"{0}\" 時發生錯誤。", InstallRetryLogFile);
                            }
                            else
                            {
                                if (!int.TryParse(LogItems[0], out LogMajroVersion) || !int.TryParse(LogItems[1], out LogMinorVersion))
                                {
                                    LogMajroVersion = MajorVersion;
                                    LogMinorVersion = MinorVersion;
                                }
                                if (!int.TryParse(LogItems[2], out RetryCount))
                                {
                                    RetryCount = 0;
                                }
                                if (MajorVersion != LogMajroVersion && MinorVersion != LogMinorVersion)
                                {
                                    RetryCount = 0;
                                }
                            }
                        }

                        RetryCount++;
                        try
                        {
                            // MajorVersion,MinorVersion,RetryCount
                            File.WriteAllText(InstallRetryLogFile, string.Format("{0},{1},{2}", MajorVersion, MinorVersion, RetryCount));
                        }
                        catch (Exception eXc)
                        {
                            Log.TraceException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_WriteRetryLogException, eXc, "寫入 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                            Log.LogException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_WriteRetryLogException, eXc, "寫入 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                        }
                    }
                    else
                    {
                        try
                        {
                            File.Delete(InstallRetryLogFile);
                        }
                        catch (Exception eXc)
                        {
                            Log.TraceException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_DeleteRetryLogException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                            Log.LogException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_DeleteRetryLogException, eXc, "刪除 \"{0}\" 時發生例外錯誤。", InstallRetryLogFile);
                        }
                    }
                }


                //7. 無論是否更新成功，回報更新版本資訊後結果給Server。
                string ComputerNetbiosName = Environment.MachineName;
                WebServiceAutoUpdate.ReportUpdateVersionResult(ComputerNetbiosName, ApplicationID, ApplicationName, MajorVersion, MinorVersion, UpdateResult, UpdateExitCode);
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_UpdateApplicationException, e, "更新程序時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.ApplicationUpdateProcess_UpdateApplicationException, e, "更新程序時發生例外錯誤。");
                return;
            }
            finally
            {

            }
        }
        #endregion
    }
}
