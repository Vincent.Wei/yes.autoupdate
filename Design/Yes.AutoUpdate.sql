/*    此 SQL DDL script 是由 Microsoft Visual Studio (發行日期: LOCAL BUILD) 產生的。                    */

/*    使用的驅動程式 : Microsoft Visual Studio - Microsoft SQL Server 驅動程式。                          */
/*    文件    : C:\Projects\Yes.AutoUpdate\Yes.AutoUpdate\Design\Yes.AutoUpdate.vsd.            */
/*    建立時間: 2012年7月14日 02:49 下午.                                                              */
/*    作業   : 從 Visio 產生資料庫精靈。                                                                 */
/*    連接的資料來源 : 未連接。                                                                          */
/*    連接的伺服器     : 未連接。                                                                       */
/*    連接的資料庫    : 不適用。                                                                        */



SET QUOTED_IDENTIFIER ON

go


/* 建立 Yes.AutoUpdate 資料庫。                                                                     */
use master  

go

create database "Yes.AutoUpdate"  

go

use "Yes.AutoUpdate"  

go

/* 建立新表格 "ApplicationDownloadPoint"。                                                          */
/* "ApplicationDownloadPoint" : 應用程式載點 的表格                                                    */
/* 	"StartIP" : 起始IP 識別 應用程式載點                                                                */
/* 	"EndIP" : 結束IP 部份識別 應用程式載點                                                                */
/* 	"DownloadPoint" : 下載點 為 應用程式載點                                                            */  
create table "ApplicationDownloadPoint" ( 
	"StartIP" bigint not null,
	"EndIP" bigint not null,
	"DownloadPoint" nvarchar(256) not null)  

go

alter table "ApplicationDownloadPoint"
	add constraint "ApplicationDownloadPoint_PK" primary key ("StartIP", "EndIP")   


go

/* 建立新表格 "ApplicationClientVersionHistory"。                                                   */
/* "ApplicationClientVersionHistory" : 應用程式用戶端版本歷史記錄 的表格                                      */
/* 	"ComputerNetbiosName" : 電腦名稱(Netbios Name) 識別 應用程式用戶端版本歷史記錄                               */
/* 	"ApplicationID" : 應用程式編號 部份識別 應用程式用戶端版本歷史記錄                                               */
/* 	"DateTime" : 更新時間 部份識別 應用程式用戶端版本歷史記錄                                                      */
/* 	"ApplicationName" : 應用程式名稱 為 應用程式用戶端版本歷史記錄                                                */
/* 	"MajorVersion" : 應用程式主要版本號 為 應用程式用戶端版本歷史記錄                                                */
/* 	"MinorVersion" : 應用程式次要版本號 為 應用程式用戶端版本歷史記錄                                                */
/* 	"UpdateResult" : 更新結果(是否成功) 為 應用程式用戶端版本歷史記錄                                               */
/* 	"UpdateExitCode" : 更新程式結束回傳值 為 應用程式用戶端版本歷史記錄                                              */  
create table "ApplicationClientVersionHistory" ( 
	"ComputerNetbiosName" nvarchar(64) not null,
	"ApplicationID" uniqueidentifier not null,
	"DateTime" datetime not null,
	"ApplicationName" nvarchar(64) not null,
	"MajorVersion" smallint not null,
	"MinorVersion" smallint not null,
	"UpdateResult" bit not null,
	"UpdateExitCode" int not null)  

go

alter table "ApplicationClientVersionHistory"
	add constraint "ApplicationClientVersionHistory_PK" primary key ("ComputerNetbiosName", "ApplicationID", "DateTime")   


go

/* 建立新表格 "ApplicationClientVersion"。                                                          */
/* "ApplicationClientVersion" : 應用程式用戶端版本 的表格                                                 */
/* 	"ComputerNetbiosName" : 電腦名稱(Netbios Name) 識別 應用程式用戶端版本                                   */
/* 	"ApplicationID" : 應用程式編號 部份識別 應用程式用戶端版本                                                   */
/* 	"ApplicationName" : 應用程式名稱 為 應用程式用戶端版本                                                    */
/* 	"MajorVersion" : 應用程式主要版本號 為 應用程式用戶端版本                                                    */
/* 	"MinorVersion" : 應用程式次要版本號 為 應用程式用戶端版本                                                    */
/* 	"LastModifyTime" : 前次修改時間 為 應用程式用戶端版本                                                     */
/* 	"LastAutoUpdateHeartbeatTime" : 前次自動更新程式回報時間 為 應用程式用戶端版本                                  */
/* 	"LastApplicationHeartbeatTime" : 前次應用程式回報時間 為 應用程式用戶端版本                                   */  
create table "ApplicationClientVersion" ( 
	"ComputerNetbiosName" nvarchar(64) not null,
	"ApplicationID" uniqueidentifier not null,
	"ApplicationName" nvarchar(64) not null,
	"MajorVersion" smallint not null,
	"MinorVersion" smallint not null,
	"LastModifyTime" datetime not null,
	"LastAutoUpdateHeartbeatTime" datetime not null,
	"LastApplicationHeartbeatTime" datetime null)  

go

alter table "ApplicationClientVersion"
	add constraint "ApplicationClientVersion_PK" primary key ("ComputerNetbiosName", "ApplicationID")   


go

/* 建立新表格 "ApplicationAutoUpdateSetting"。                                                      */
/* "ApplicationAutoUpdateSetting" : 應用程式自動更新設定 的表格                                            */
/* 	"ApplicationID" : 應用程式編號 識別 應用程式自動更新設定                                                    */
/* 	"ApplicationName" : 應用程式名稱 為 應用程式自動更新設定                                                   */
/* 	"ApplicationDescription" : 應用程式描述 為 應用程式自動更新設定                                            */
/* 	"ApplicationType" : 應用程式類型 為 應用程式自動更新設定                                                   */
/* 	"MajorVersion" : 應用程式主要版本號 為 應用程式自動更新設定                                                   */
/* 	"MinorVersion" : 應用程式次要版本號 為 應用程式自動更新設定                                                   */
/* 	"IsNeedReportVersion" : 是否必須定期回報版本 為 應用程式自動更新設定                                           */
/* 	"VersionHash" : 版本資訊雜湊值 為 應用程式自動更新設定                                                      */
/* 	"DownloadUrl" : 下載位址 為 應用程式自動更新設定                                                         */
/* 	"UpdateWaitingMinutes" : 等待更新最大分鐘數 為 應用程式自動更新設定                                           */
/* 	"CreateTime" : 建立時間 為 應用程式自動更新設定                                                          */
/* 	"LastModifyTime" : 前次修改時間 為 應用程式自動更新設定                                                    */  
create table "ApplicationAutoUpdateSetting" ( 
	"ApplicationID" uniqueidentifier not null,
	"ApplicationName" nvarchar(64) not null,
	"ApplicationDescription" nvarchar(256) null,
	"ApplicationType" tinyint not null,
	"MajorVersion" smallint not null,
	"MinorVersion" smallint not null,
	"IsNeedReportVersion" bit not null,
	"VersionHash" nvarchar(256) not null,
	"DownloadUrl" nvarchar(256) not null,
	"UpdateWaitingMinutes" int not null,
	"CreateTime" datetime not null,
	"LastModifyTime" datetime not null)  

go

alter table "ApplicationAutoUpdateSetting"
	add constraint "ApplicationAutoUpdateSetting_PK" primary key ("ApplicationID")   


go


/* 這是 Microsoft Visual Studio 產生之 SQL DDL script 的結尾。                                         */
