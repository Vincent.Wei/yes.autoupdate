﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Yes.AutoUpdate.Server.Business
{
    public class Application : Base
    {
        #region Property

        #region ID
        /// <summary>
        /// 應用程式ID。
        /// </summary>
        private Guid _ID = Guid.Empty;
        private string _IDString = string.Empty;
        /// <summary>
        /// 取得或設定應用程式ID。
        /// </summary>
        /// <value>應用程式ID。</value>
        public Guid ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
                _IDString = value.ToString("D");
            }
        }
        #endregion

        #region Name
        /// <summary>
        /// 應用程式名稱。
        /// </summary>
        private string _Name = string.Empty;
        /// <summary>
        /// 取得或設定應用程式名稱。
        /// </summary>
        /// <value>應用程式名稱。</value>
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        #endregion

        #region Description
        /// <summary>
        /// 應用程式備註。
        /// </summary>
        private string _Description = string.Empty;
        /// <summary>
        /// 取得或設定應用程式備註。
        /// </summary>
        /// <value>應用程式備註。</value>
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
            }
        }
        #endregion

        #region VersionHash
        /// <summary>
        /// 應用程式版本雜湊值。
        /// </summary>
        [System.Xml.Serialization.XmlIgnore]
        private string _VersionHash = string.Empty;
        /// <summary>
        /// 取得或設定應用程式版本雜湊值。
        /// </summary>
        /// <value>應用程式版本雜湊值。</value>
        [System.Xml.Serialization.XmlIgnore]
        public string VersionHash
        {
            get
            {
                return _VersionHash;
            }
            set
            {
                _VersionHash = value;
            }
        }
        #endregion

        #region MajorVersion
        /// <summary>
        /// 應用程式主要版本。
        /// </summary>
        private short _MajorVersion = short.MinValue;
        /// <summary>
        /// 取得或設定應用程式主要版本。
        /// </summary>
        /// <value>應用程式主要版本。</value>
        public short MajorVersion
        {
            get
            {
                return _MajorVersion;
            }
            set
            {
                _MajorVersion = value;
            }
        }
        #endregion

        #region MinorVersion
        /// <summary>
        /// 應用程式次要版本。
        /// </summary>
        private short _MinorVersion = short.MinValue;
        /// <summary>
        /// 取得或設定應用程式次要版本。
        /// </summary>
        /// <value>應用程式次要版本。</value>
        public short MinorVersion
        {
            get
            {
                return _MinorVersion;
            }
            set
            {
                _MinorVersion = value;
            }
        }
        #endregion

        #region ApplicationType
        /// <summary>
        /// 應用程式類型。
        /// </summary>
        private ApplicationTypeId _ApplicationType = ApplicationTypeId.None;
        /// <summary>
        /// 取得或設定應用程式類型。
        /// </summary>
        /// <value>應用程式類型。</value>
        public ApplicationTypeId ApplicationType
        {
            get
            {
                return _ApplicationType;
            }
            set
            {
                _ApplicationType = value;
            }
        }
        #endregion

        #region BackupExcludeSubfolders
        /// <summary>
        /// 應用程式備份例外資料夾。
        /// </summary>
        private string[] _BackupExcludeSubfolders = null;
        /// <summary>
        /// 取得或設定應用程式備份例外資料夾。
        /// </summary>
        /// <value>應用程式備份例外資料夾。</value>
        public string[] BackupExcludeSubfolders
        {
            get
            {
                return _BackupExcludeSubfolders;
            }
            set
            {
                _BackupExcludeSubfolders = value;
            }
        }
        #endregion

        #region IsNeedReportVersion
        /// <summary>
        /// 是否必須由應用程式本身定期回報版本。
        /// </summary>
        private bool _IsNeedReportVersion = false;
        /// <summary>
        /// 取得或設定是否必須由應用程式本身定期回報版本。
        /// </summary>
        /// <value>是否必須由應用程式本身定期回報版本。</value>
        public bool IsNeedReportVersion
        {
            get
            {
                return _IsNeedReportVersion;
            }
            set
            {
                _IsNeedReportVersion = value;
            }
        }
        #endregion

        #endregion
    }
}
