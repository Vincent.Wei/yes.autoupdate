﻿using System;

namespace Yes.AutoUpdate.Server.Business
{
    public enum MessageId
    {
        #region UpdateApplicationVersion (100xx)
        /// <summary>
        /// 更新檔存放根目錄 \"{0}\" 不存在。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_UpdateBaseFolderNotExist = 10001,
        /// <summary>
        /// 壓縮檔 \"{0}\" 不存在。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_UpdateZipFileNotExist = 10002,
        /// <summary>
        /// 建立暫存目錄失敗，\"{0}\"。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_CreateTempFolderFail = 10003,
        /// <summary>
        /// 必要檔案 \"{0}\" 不存在。\r\n
        /// 版本資訊檔 \"{0}\" 不存在。\r\n
        /// </summary>
        ApplicationAutoUpdateSettingHelper_FileNotExist = 10004,
        /// <summary>
        /// 解析版本資訊檔 \"{0}\"時發生例外錯誤。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_ParseVersionXmlFail = 10005,
        /// <summary>
        /// 更新資料庫設定失敗。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_UpdateDataBaseFail = 10006,
        /// <summary>
        /// 更新資料庫設定時發生例外錯誤。
        /// </summary>
        ApplicationAutoUpdateSettingHelper_UpdateApplicationVersionException = 10007,
        #endregion
    }
}
