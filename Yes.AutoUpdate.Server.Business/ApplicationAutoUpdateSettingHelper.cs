﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Security.Cryptography;
using Ionic.Zip;
using System.Xml;
using System.Configuration;
using System.Data.SqlClient;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Server.Business
{
    public class ApplicationAutoUpdateSettingHelper : Base
    {
        public bool ListApplications(out DataTable ResultTable)
        {
            ResultTable = null;
            return true;
        }

        public bool GetApplication(Guid AppliationID, out DataTable ResultTable)
        {
            ResultTable = null;
            return true;
        }

        #region UpdateApplicationVersion
        /// <summary>
        /// 更新資料庫設定。
        /// 1. 檢查檔案、壓縮檔、必要檔案是否存在。
        /// 2. 解析版本資訊檔。
        /// 3. 複製檔案至指定目錄。
        /// 4. 更新資料庫設定。
        /// </summary>
        /// <param name="DownloadBaseUrl">檔案下載位置。</param>
        /// <param name="UpdateBaseFolderPaths">更新檔存放根目錄。</param>
        /// <param name="VersionFileName">版本檔檔名。</param>
        /// <param name="CheckExistFileNames">必要檔案。</param>
        /// <param name="UpdateZipFileName">更新壓縮檔。</param>
        /// <param name="UpdateWaitingMinutes">更新等待時間。</param>
        /// <param name="ErrorMessage">錯誤訊息。</param>
        /// <returns>是否成功。</returns>
        public bool UpdateApplicationVersion(string DownloadBaseUrl, string[] UpdateBaseFolderPaths, string VersionFileName, string[] CheckExistFileNames, string UpdateZipFileName, int UpdateWaitingMinutes, out string ErrorMessage)
        {
            bool ReturnResult = false;
            ErrorMessage = string.Empty;

            //暫存目錄。
            string TempFolder = Path.GetTempPath() + "\\Yes.AutoUpdate\\";
            //檔案格式
            string FileNameFormat = "{0}.{1}.zip";

            System.Data.SqlClient.SqlConnection _Connection = null;

            try
            {
                Log.TraceMethodEntry(LogSourceName, "DownloadBaseUrl: \"{0}\"; UpdateBaseFolderPathsCount: \"{1}\"; VersionFileName: \"{2}\"; CheckExistFileNamesCount: \"{3}\"; UpdateZipFileName: \"{4}\"; UpdateWaitingMinutes: \"{5}\"; "
                    , DownloadBaseUrl, UpdateBaseFolderPaths.Length, VersionFileName, CheckExistFileNames.Length, UpdateZipFileName, UpdateWaitingMinutes);

                int _CommandTimeout = Convert.ToInt16(ConfigurationManager.AppSettings["Timeout"]);
                string _ConnectionString = ConfigurationManager.ConnectionStrings["Yes.AutoUpdate.Database"].ConnectionString;

                #region 檢查更新檔目錄是否存在。
                foreach (string UpdateBaseFolder in UpdateBaseFolderPaths)
                {
                    if (!Directory.Exists(UpdateBaseFolder))
                    {
                        //目錄不存在。
                        ErrorMessage = string.Format("更新檔存放根目錄 \"{0}\" 不存在。", UpdateBaseFolder);

                        Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateBaseFolderNotExist, ErrorMessage);
                        Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateBaseFolderNotExist, ErrorMessage);
                        ReturnResult = false;
                        return ReturnResult;
                    }
                }
                #endregion

                #region 檢查壓縮檔是否存在。
                if (!File.Exists(UpdateZipFileName))
                {
                    //檔案不存在。
                    ErrorMessage = string.Format("壓縮檔 \"{0}\" 不存在。", UpdateZipFileName);

                    Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateZipFileNotExist, ErrorMessage);
                    Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateZipFileNotExist, ErrorMessage);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                #region 解壓縮壓縮檔。 (檢查必要檔案、版本資訊檔)
                // 檢查並建立目標輸出路徑
                if (!Directory.Exists(TempFolder))
                {
                    Directory.CreateDirectory(TempFolder);

                    if (!Directory.Exists(TempFolder))
                    {
                        //暫存目錄不存在，且無法建立目錄。
                        ErrorMessage = string.Format("建立暫存目錄失敗，\"{0}\"。", TempFolder);

                        Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_CreateTempFolderFail, ErrorMessage);
                        Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_CreateTempFolderFail, ErrorMessage);

                        ReturnResult = false;
                        return ReturnResult;
                    }
                }

                List<string> test = new List<string>(CheckExistFileNames);

                using (Ionic.Zip.ZipFile zip = ZipFile.Read(UpdateZipFileName))
                {
                    foreach (string CheckExistFileName in CheckExistFileNames)
                    {
                        ZipEntry GetFile = zip[CheckExistFileName.Replace("\\", "/")];
                        if (GetFile == null)
                        {
                            ErrorMessage += string.Format("必要檔案 \"{0}\" 不存在。\r\n", CheckExistFileName);
                        }

                    }
                    ZipEntry GetVersionFile = zip[VersionFileName.Replace("\\", "/")];
                    if (GetVersionFile == null)
                    {
                        ErrorMessage += string.Format("版本資訊檔 \"{0}\" 不存在。\r\n", VersionFileName);
                    }
                    else
                    {
                        GetVersionFile.Extract(TempFolder, true);
                    }

                }
                #endregion

                if (ErrorMessage != string.Empty)
                {
                    Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_FileNotExist, ErrorMessage);
                    Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_FileNotExist, ErrorMessage);

                    ReturnResult = false;
                    return ReturnResult;
                }

                Application VersionInfomation = null;
                #region 解析版本資訊檔。
                try
                {
                    string VersionXML = File.ReadAllText(TempFolder + VersionFileName, Encoding.UTF8);


                    VersionInfomation = (Application)Base.XmlDeserialize(VersionXML, typeof(Application));

                    VersionInfomation.VersionHash = GetMD5Hash(VersionXML);
                }
                catch
                {
                    ErrorMessage = string.Format("解析版本資訊檔 \"{0}\"時發生例外錯誤。", VersionFileName);

                    Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_ParseVersionXmlFail, ErrorMessage);
                    Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_ParseVersionXmlFail, ErrorMessage);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion


                if (!DownloadBaseUrl.EndsWith("/"))
                {
                    DownloadBaseUrl += "/";
                }

                string FileName = string.Format(FileNameFormat, VersionInfomation.MajorVersion, VersionInfomation.MinorVersion);
                string DownloadUrl = DownloadBaseUrl + VersionInfomation.ID.ToString("D") + "/" + FileName;

                #region 複製檔案至指定目錄。
                string UpdatePath = string.Empty;
                string UpdateFilePath = string.Empty;

                foreach (string UpdateBaseFolderPath in UpdateBaseFolderPaths)
                {
                    UpdatePath = UpdateBaseFolderPath;

                    if (!UpdatePath.EndsWith("\\"))
                    {
                        UpdatePath += "\\";
                    }

                    UpdatePath += VersionInfomation.ID.ToString("D") + "\\";
                    UpdateFilePath = UpdatePath + FileName;
                    if (!Directory.Exists(UpdatePath))
                    {
                        Directory.CreateDirectory(UpdatePath);
                    }

                    File.Copy(UpdateZipFileName, UpdateFilePath, true);
                }
                #endregion

                #region 更新資料庫設定。
                //最後更新時間。
                DateTime Now = DateTime.Now;

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();

                //資料庫是否有此設定資料。
                bool IsExist = false;
                #region IsExist

                #region IsExistSqlCommandString
                string IsExistSqlCommandString =
                    "SELECT DISTINCT "
                    + " ApplicationID "
                    + " FROM ApplicationAutoUpdateSetting"
                    + " WHERE ApplicationID = @ApplicationID";
                #endregion

                System.Data.SqlClient.SqlCommand IsExistCommand = _Connection.CreateCommand();
                IsExistCommand.CommandTimeout = _CommandTimeout;
                IsExistCommand.CommandType = System.Data.CommandType.Text;
                IsExistCommand.CommandText = IsExistSqlCommandString;

                #region Parameters
                IsExistCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = VersionInfomation.ID.ToString("D");
                #endregion

                SqlDataAdapter Adapter = new SqlDataAdapter(IsExistCommand);
                DataTable TempTable = new DataTable();
                if (Adapter.Fill(TempTable) != 0)
                {
                    //無資料。
                    IsExist = true;
                }
                else
                {
                    //有資料。
                    IsExist = false;
                }

                TempTable = null;
                #endregion

                #region SqlCommandString
                string ApplicationAutoUpdateSettingSqlCommandString = string.Empty;
                if (IsExist)
                {
                    //有資料則更新。
                    ApplicationAutoUpdateSettingSqlCommandString =
                        " UPDATE ApplicationAutoUpdateSetting "
                        + " SET ApplicationName = @ApplicationName, "
                        + " ApplicationDescription = @ApplicationDescription, "
                        + " ApplicationType = @ApplicationType, "
                        + " MajorVersion = @MajorVersion, "
                        + " MinorVersion = @MinorVersion, "
                        + " IsNeedReportVersion = @IsNeedReportVersion, "
                        + " VersionHash = @VersionHash, "
                        + " DownloadUrl = @DownloadUrl, "
                        + " UpdateWaitingMinutes = @UpdateWaitingMinutes, "
                        + " LastModifyTime = @LastModifyTime "
                        + " WHERE ApplicationID = @ApplicationID ";

                }
                else
                {
                    //無資料則新增。
                    ApplicationAutoUpdateSettingSqlCommandString =
                        "INSERT INTO ApplicationAutoUpdateSetting ( "
                        + " ApplicationID, "
                        + " ApplicationName, "
                        + " ApplicationDescription, "
                        + " ApplicationType, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " IsNeedReportVersion, "
                        + " VersionHash, "
                        + " DownloadUrl, "
                        + " UpdateWaitingMinutes, "
                        + " CreateTime, "
                        + " LastModifyTime "
                        + " ) "
                        + " VALUES ( "
                        + " @ApplicationID, "
                        + " @ApplicationName, "
                        + " @ApplicationDescription, "
                        + " @ApplicationType, "
                        + " @MajorVersion, "
                        + " @MinorVersion, "
                        + " @IsNeedReportVersion, "
                        + " @VersionHash, "
                        + " @DownloadUrl, "
                        + " @UpdateWaitingMinutes, "
                        + " @CreateTime, "
                        + " @LastModifyTime "
                        + " ) ";
                }
                #endregion

                // Prepare Command
                System.Data.SqlClient.SqlCommand ApplicationAutoUpdateSettingCommand = _Connection.CreateCommand();
                ApplicationAutoUpdateSettingCommand.CommandTimeout = _CommandTimeout;
                ApplicationAutoUpdateSettingCommand.CommandType = System.Data.CommandType.Text;
                ApplicationAutoUpdateSettingCommand.CommandText = ApplicationAutoUpdateSettingSqlCommandString;

                #region Parameters
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = VersionInfomation.ID.ToString("D");
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = VersionInfomation.Name;
                if (string.IsNullOrEmpty(VersionInfomation.Description))
                {
                    ApplicationAutoUpdateSettingCommand.Parameters.Add("@ApplicationDescription", SqlDbType.NVarChar).Value = DBNull.Value;
                }
                else
                {
                    ApplicationAutoUpdateSettingCommand.Parameters.Add("@ApplicationDescription", SqlDbType.NVarChar).Value = VersionInfomation.Description;
                }
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@ApplicationType", SqlDbType.TinyInt).Value = (short)(int)VersionInfomation.ApplicationType;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@MajorVersion", SqlDbType.SmallInt).Value = VersionInfomation.MajorVersion;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@MinorVersion", SqlDbType.SmallInt).Value = VersionInfomation.MinorVersion;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@IsNeedReportVersion", SqlDbType.Bit).Value = VersionInfomation.IsNeedReportVersion;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@VersionHash", SqlDbType.NVarChar).Value = VersionInfomation.VersionHash;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@DownloadUrl", SqlDbType.NVarChar).Value = DownloadUrl;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@UpdateWaitingMinutes", SqlDbType.Int).Value = UpdateWaitingMinutes;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@CreateTime", SqlDbType.DateTime).Value = Now;
                ApplicationAutoUpdateSettingCommand.Parameters.Add("@LastModifyTime", SqlDbType.DateTime).Value = Now;
                #endregion

                // Execute
                if (ApplicationAutoUpdateSettingCommand.ExecuteNonQuery() < 0)
                {
                    ErrorMessage = "更新資料庫設定失敗。";

                    Log.TraceError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateDataBaseFail, ErrorMessage);
                    Log.LogError(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateDataBaseFail, ErrorMessage);

                    ReturnResult = false;
                    return ReturnResult;
                }
                #endregion

                ReturnResult = true;
                return ReturnResult;
            }
            catch (Exception e)
            {
                ErrorMessage = string.Format("發生例外錯誤：{0}", e);

                Log.TraceException(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateApplicationVersionException, e, "更新資料庫設定時發生例外錯誤。");
                Log.LogException(LogSourceName, (int)MessageId.ApplicationAutoUpdateSettingHelper_UpdateApplicationVersionException, e, "更新資料庫設定時發生例外錯誤。");

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                #region 關閉SQL資料庫連線。
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                        _Connection.Dispose();
                        _Connection = null;
                    }
                }
                catch
                {
                }
                #endregion

                #region 刪除暫存目錄。
                try
                {
                    Directory.Delete(TempFolder, true);
                }
                catch
                {
                }
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}, ErrorMessage: {1}", ReturnResult, ErrorMessage == string.Empty ? "Empty" : ErrorMessage);
            }
        }

        #region GetMD5Hash
        /// <summary>
        /// 產生雜湊值(MD5)。
        /// </summary>
        /// <param name="input">字串</param>
        /// <returns>雜湊值(MD5)</returns>
        private static string GetMD5Hash(string input)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
        #endregion

        #endregion
    }
}
