﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Xml;
using Yes.Framework.Helper;

namespace Yes.AutoUpdate.Web
{
    /// <summary>
    /// AutoUpdateService 的摘要描述
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允許使用 ASP.NET AJAX 從指令碼呼叫此 Web 服務，請取消註解下一行。
    // [System.Web.Script.Services.ScriptService]
    public class AutoUpdateService : System.Web.Services.WebService
    {
        /// <summary>
        /// 追蹤來源名稱。
        /// </summary>
        protected static string LogSourceName = "Yes.AutoUpdate.Web";
        /// <summary>
        /// 交易逾時秒數。
        /// </summary>
        protected static int _CommandTimeout = int.Parse(ConfigurationManager.AppSettings["Timeout"]);
        /// <summary>
        /// 資料庫連接字串。
        /// </summary>
        protected static string _ConnectionString = ConfigurationManager.ConnectionStrings["Yes.AutoUpdate.Database"].ConnectionString;


        #region GetRequestClientIP
        /// <summary>
        /// 取得用戶端IP。
        /// </summary>
        /// <param name="Request">Http Request。</param>
        /// <returns>用戶端IP。</returns>
        protected static ulong GetRequestClientIP(System.Web.HttpRequest Request)
        {
            ulong ReturnResult = 0;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);


                // 若透過 Proxy，則不保證能取得 Client IP，以下數種 HEADER 為可能的 Proxy Header。
                if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_CLIENT_IP"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["HTTP_CLIENT_IP"]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED_FOR"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(',')[0]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_FORWARDED"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["HTTP_X_FORWARDED"]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_FORWARDED_FOR"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["HTTP_FORWARDED_FOR"]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                if (!string.IsNullOrEmpty(Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                if (!string.IsNullOrEmpty(Request.ServerVariables["REMOTE_ADDR"]))
                {
                    ReturnResult = IP2Long(Request.ServerVariables["REMOTE_ADDR"]);
                }
                if (ReturnResult != 0)
                {
                    return ReturnResult;
                }

                ReturnResult = 0;
                return ReturnResult;
            }
            catch (Exception e)
            {
                //Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_GetRequestClientIPException, e, "取得用戶端IP時發生例外錯誤。");
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_GetRequestClientIPException, e, "取得用戶端IP時發生例外錯誤。");

                ReturnResult = 0;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }

        #region IP2Long
        /// <summary>
        /// 
        /// </summary>
        /// <param name="IP"></param>
        /// <returns></returns>
        private static ulong IP2Long(string IP)
        {
            ulong ReturnResult = 0;
            try
            {
                Log.TraceMethodEntry(LogSourceName, "IP:{0}", IP);

                if (string.IsNullOrEmpty(IP))
                {
                    ReturnResult = 0;
                    return ReturnResult;
                }

                string[] SubnetArray = IP.Split('.');
                if (SubnetArray.Length != 4)
                {
                    ReturnResult = 0;
                    return ReturnResult;
                }

                ulong IP1, IP2, IP3, IP4;
                if (ulong.TryParse(SubnetArray[0], out IP1)
                    && ulong.TryParse(SubnetArray[1], out IP2)
                    && ulong.TryParse(SubnetArray[2], out IP3)
                    && ulong.TryParse(SubnetArray[3], out IP4))
                {
                    ReturnResult = IP1 * 1000000000 + IP2 * 1000000 + IP3 * 1000 + IP4;
                }
                else
                {
                    ReturnResult = 0;
                }

                return ReturnResult;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_GetRequestClientIPException, e, "取得用戶端IP時發生例外錯誤。");

                ReturnResult = 0;
                return ReturnResult;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #endregion


        #region GetApplicationVersion
        /// <summary>
        /// 取得所有應用程式最新版本資訊，回傳DataTable，若執行失敗則回傳null。
        /// 1.取得最新版本Hash值。
        /// 2.回傳DataTable。
        /// </summary>
        /// <returns>所有應用程式版本資訊 DataTable，回傳欄位 ID、VersionHash、ApplicationName、MajorVersion、MinorVersion、DownloadUrl，若執行失敗則回傳null。</returns>
        [WebMethod]
        public DataTable GetApplicationVersion()
        {
            DataTable ResultTable = null;
            System.Data.SqlClient.SqlConnection _Connection = null;
            System.Data.SqlClient.SqlDataAdapter _Adapter = null;
            System.Data.SqlClient.SqlDataReader _Reader = null;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);


                #region SqlCommandString
                string SqlCommandString =
                    " SELECT DISTINCT "
                    + " ApplicationID, "
                    + " VersionHash, "
                    + " ApplicationName, "
                    + " MajorVersion, "
                    + " MinorVersion, "
                    + " DownloadUrl "
                    + " FROM ApplicationAutoUpdateSetting ";
                #endregion

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();

                // Prepare Command
                System.Data.SqlClient.SqlCommand Command = _Connection.CreateCommand();
                Command.CommandTimeout = _CommandTimeout;
                Command.CommandType = System.Data.CommandType.Text;
                Command.CommandText = SqlCommandString;

                _Adapter = new SqlDataAdapter(Command);
                ResultTable = new DataTable();
                ResultTable.TableName = "ResultTable";
                if (_Adapter.Fill(ResultTable) == 0)
                {
                    //無資料。
                }


                ulong ClientIP = GetRequestClientIP(Context.Request);
                if (ClientIP != 0)
                {
                    Command.CommandText = "SELECT DownloadPoint FROM ApplicationDownloadPoint WHERE StartIP <= @IP AND @IP <= EndIP";
                    Command.Parameters.Clear();
                    Command.Parameters.Add("@IP", SqlDbType.BigInt).Value = ClientIP;

                    _Reader = Command.ExecuteReader();
                    if (_Reader.Read())
                    {
                        string DownloadPoint = (string)_Reader["DownloadPoint"];
                        foreach (DataRow Row in ResultTable.Rows)
                        {
                            try
                            {
                                string DownloadUrl = (string)Row["DownloadUrl"];
                                int StartIndex = DownloadUrl.IndexOf("://") + 3;
                                int EndIndex = DownloadUrl.IndexOf('/', StartIndex);
                                Row["DownloadUrl"] = string.Format("{0}{1}{2}", DownloadUrl.Substring(0, StartIndex), DownloadPoint, DownloadUrl.Substring(EndIndex));
                            }
                            catch (Exception e)
                            {
                                //Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_ConvertDownloadPointException, e, "轉換應用程式載點時發生例外錯誤。");
                                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_ConvertDownloadPointException, e, "轉換應用程式載點時發生例外錯誤。");
                            }
                        }
                    }
                    _Reader.Close();
                    _Reader.Dispose();
                    _Reader = null;
                }

                return ResultTable;
            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_GetApplicationVersionException, e, "取得所有應用程式最新版本資訊時發生例外錯誤。");
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_GetApplicationVersionException, e, "取得所有應用程式最新版本資訊時發生例外錯誤。");

                ResultTable = null;
                return ResultTable;
            }
            finally
            {
                #region Dispose
                #region Adapter.Dispose
                try
                {
                    if (_Adapter != null)
                    {
                        _Adapter.Dispose();
                    }
                }
                catch
                {
                }
                _Adapter = null;
                #endregion
                #region Reader.Dispose
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Dispose();
                    }
                }
                catch
                {
                }
                _Reader = null;
                #endregion
                #region Connestion.Dispose
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                    }
                }
                catch
                {
                }
                _Connection = null;
                #endregion
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ResultTable == null ? "null" : ResultTable.Rows.Count.ToString());
            }
        }
        #endregion

        #region GetApplicationVersionToString (for java)
        /// <summary>
        /// 取得所有應用程式最新版本資訊，回傳 DataTable 的 xml，若執行失敗則回傳空字串。
        /// 1.Call GetApplicationVersion()。
        /// 2.回傳DataTable 的 xml。
        /// </summary>
        /// <returns>所有應用程式版本資訊 DataTable 的 xml，回傳欄位 ID、VersionHash、ApplicationName、MajorVersion、MinorVersion、DownloadUrl，若執行失敗則回傳null。</returns>
        [WebMethod]
        public string GetApplicationVersionToString()
        {
            string ReturnString = string.Empty;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                DataTable ResultTable = GetApplicationVersion();
                if (ResultTable == null)
                {
                    ReturnString = string.Empty;
                    return ReturnString;
                }

                //System.IO.StringWriter DataTableStringWriter = new System.IO.StringWriter();
                //System.Xml.Serialization.XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(typeof(DataTable));
                //Serializer.Serialize(DataTableStringWriter, ResultTable);
                MemoryStream OutputStream = new MemoryStream();
                XmlWriterSettings OutputXmlWriterSettings = new XmlWriterSettings();
                OutputXmlWriterSettings.Encoding = new UTF8Encoding(false);
                OutputXmlWriterSettings.Indent = true;
                XmlWriter OutputXmlWriter = XmlWriter.Create(OutputStream, OutputXmlWriterSettings);
                System.Xml.Serialization.XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(typeof(DataTable));
                Serializer.Serialize(OutputXmlWriter, ResultTable);
                ReturnString = Encoding.UTF8.GetString(OutputStream.ToArray());

                OutputXmlWriter.Close();
                OutputXmlWriter = null;
                OutputStream.Close();
                OutputStream.Dispose();
                OutputStream = null;
                OutputXmlWriterSettings = null;
                Serializer = null;

                return ReturnString;
            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_GetApplicationVersionException, e, "取得所有應用程式最新版本資訊時發生例外錯誤。");
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_GetApplicationVersionException, e, "取得所有應用程式最新版本資訊時發生例外錯誤。");

                ReturnString = string.Empty;
                return ReturnString;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return '{0}'", ReturnString);
            }
        }
        #endregion

        #region GetNewVersion
        /// <summary>
        /// 取得指定應用程式ID的最新版本資訊，回傳XML字串，若失敗回傳空字串。
        /// 1.向資料庫取得應用程式下載資訊。
        /// 2.組成Xml格式並回傳。
        /// </summary>
        /// <param name="ApplicationID">應用程式ID</param>
        /// <returns>XML字串，若失敗回傳空字串。
        /// <Root>
        /// <ApplicationName></ApplicationName>
        /// <MajorVersion></MajorVersion>
        /// <MinorVersion></MinorVersion>
        /// <DownloadUrl></DownloadUrl>
        /// <UpdateWaitingMinutes></UpdateWaitingMinutes>
        /// </Root>
        /// </returns>
        [WebMethod]
        public string GetNewVersion(string ApplicationID)
        {
            string ReturnString = string.Empty;

            System.Data.SqlClient.SqlConnection _Connection = null;
            System.Data.SqlClient.SqlDataReader _Reader = null;

            #region XmlTag
            const string RootNodeName = "Root";
            const string ApplicationNameNodeName = "ApplicationName";
            const string MajorVersionNodeName = "MajorVersion";
            const string MinorVersionNodeName = "MinorVersion";
            const string DownloadUrlNodeName = "DownloadUrl";
            const string UpdateWaitingMinutesNodeName = "UpdateWaitingMinutes";
            #endregion

            string ResultXmlString = string.Empty;
            XmlDocument XmlDoc = null;
            MemoryStream OutputStream = null;
            XmlTextWriter OutputXmlWriter = null;
            StreamReader OutputStreamReader = null;

            string ApplicationName = string.Empty;
            short MajorVersion = short.MinValue;
            short MinorVersion = short.MinValue;
            string DownloadUrl = string.Empty;
            int UpdateWaitingMinutes = int.MinValue;

            try
            {
                Log.TraceMethodEntry(LogSourceName, "ApplicationID：'{0}';", ApplicationID);

                #region SqlCommandString
                string SqlCommandString =
                    " SELECT DISTINCT "
                    + " ApplicationName, "
                    + " MajorVersion, "
                    + " MinorVersion, "
                    + " DownloadUrl, "
                    + " UpdateWaitingMinutes "
                    + " FROM ApplicationAutoUpdateSetting "
                    + " WHERE ApplicationID = @ApplicationID ";
                #endregion

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();

                // Prepare Command
                System.Data.SqlClient.SqlCommand Command = _Connection.CreateCommand();
                Command.CommandTimeout = _CommandTimeout;
                Command.CommandType = System.Data.CommandType.Text;
                Command.CommandText = SqlCommandString;

                #region WHERE
                Command.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                #endregion

                _Reader = Command.ExecuteReader();

                if (_Reader.Read())
                {
                    ApplicationName = (string)_Reader["ApplicationName"];
                    MajorVersion = (short)_Reader["MajorVersion"];
                    MinorVersion = (short)_Reader["MinorVersion"];
                    DownloadUrl = (string)_Reader["DownloadUrl"];
                    UpdateWaitingMinutes = (int)_Reader["UpdateWaitingMinutes"];
                }
                _Reader.Close();
                _Reader.Dispose();
                _Reader = null;

                #region ConvertDownloadPoint
                ulong ClientIP = GetRequestClientIP(Context.Request);
                if (ClientIP != 0)
                {
                    Command.CommandText = "SELECT DownloadPoint FROM ApplicationDownloadPoint WHERE StartIP <= @IP AND @IP <= EndIP";
                    Command.Parameters.Clear();
                    Command.Parameters.Add("@IP", SqlDbType.BigInt).Value = ClientIP;

                    _Reader = Command.ExecuteReader();
                    if (_Reader.Read())
                    {
                        string DownloadPoint = (string)_Reader["DownloadPoint"];
                            try
                            {
                                int StartIndex = DownloadUrl.IndexOf("://") + 3;
                                int EndIndex = DownloadUrl.IndexOf('/', StartIndex);
                                DownloadUrl = string.Format("{0}{1}{2}", DownloadUrl.Substring(0, StartIndex), DownloadPoint, DownloadUrl.Substring(EndIndex));
                            }
                            catch (Exception e)
                            {
                                //Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_ConvertDownloadPointException, e, "轉換應用程式載點時發生例外錯誤。");
                                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_ConvertDownloadPointException, e, "轉換應用程式載點時發生例外錯誤。");
                            }
                    }
                    _Reader.Close();
                    _Reader.Dispose();
                    _Reader = null;
                }
                #endregion

                #region 組 Xml
                XmlDoc = new XmlDocument();
                XmlElement XmlRootNode = XmlDoc.CreateElement(RootNodeName);
                XmlDoc.AppendChild(XmlRootNode);

                XmlElement SubXmlNode = XmlDoc.CreateElement(ApplicationNameNodeName);
                SubXmlNode.InnerText = ApplicationName;
                XmlRootNode.AppendChild(SubXmlNode);

                SubXmlNode = XmlDoc.CreateElement(MajorVersionNodeName);
                SubXmlNode.InnerText = MajorVersion.ToString();
                XmlRootNode.AppendChild(SubXmlNode);

                SubXmlNode = XmlDoc.CreateElement(MinorVersionNodeName);
                SubXmlNode.InnerText = MinorVersion.ToString();
                XmlRootNode.AppendChild(SubXmlNode);

                SubXmlNode = XmlDoc.CreateElement(DownloadUrlNodeName);
                SubXmlNode.InnerText = DownloadUrl;
                XmlRootNode.AppendChild(SubXmlNode);

                SubXmlNode = XmlDoc.CreateElement(UpdateWaitingMinutesNodeName);
                SubXmlNode.InnerText = UpdateWaitingMinutes.ToString();
                XmlRootNode.AppendChild(SubXmlNode);
                #endregion

                #region XmlDocument to String
                OutputStream = new MemoryStream();
                OutputXmlWriter = new XmlTextWriter(OutputStream, System.Text.Encoding.UTF8);
                OutputXmlWriter.Formatting = Formatting.Indented;
                XmlDoc.Save(OutputXmlWriter);

                XmlDoc = null;


                OutputStreamReader = new StreamReader(OutputStream, System.Text.Encoding.UTF8);
                OutputStream.Position = 0;
                ResultXmlString = OutputStreamReader.ReadToEnd();

                OutputXmlWriter.Close();
                OutputXmlWriter = null;
                OutputStreamReader.Close();
                OutputStreamReader.Dispose();
                OutputStreamReader = null;
                OutputStream.Close();
                OutputStream.Dispose();
                OutputStream = null;
                #endregion


                ReturnString = ResultXmlString;
                return ReturnString;
            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_GetNewVersionException, e, "取得指定應用程式ID的最新版本資訊時發生例外錯誤，ApplicationID：'{0}'。", ApplicationID);
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_GetNewVersionException, e, "取得指定應用程式ID的最新版本資訊時發生例外錯誤，ApplicationID：'{0}'。", ApplicationID);

                ReturnString = string.Empty;
                return ReturnString;
            }
            finally
            {
                #region Clean
                try
                {
                    if (XmlDoc != null)
                    {
                        XmlDoc = null;
                    }
                    if (OutputXmlWriter != null)
                    {
                        OutputXmlWriter.Close();
                        OutputXmlWriter = null;
                    }
                    if (OutputStreamReader != null)
                    {
                        OutputStreamReader.Close();
                        OutputStreamReader.Dispose();
                        OutputStreamReader = null;
                    }
                    if (OutputStream != null)
                    {
                        OutputStream.Close();
                        OutputStream.Dispose();
                        OutputStream = null;
                    }
                }
                catch
                {
                }
                #endregion

                #region Dispose
                #region Reader.Dispose
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Dispose();
                    }
                }
                catch
                {
                }
                _Reader = null;
                #endregion
                #region Connection.Dispose
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                    }
                }
                catch
                {
                }
                _Connection = null;
                #endregion
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnString == string.Empty ? "null" : ReturnString);
            }
        }
        #endregion

        #region ReportUpdateVersionResult
        /// <summary>
        /// 接收Client更新版本資訊後結果。
        /// 1. 應用程式名稱,版本號無值則取得最新版本資訊。
        /// 2. 檢查資料庫是否有此電腦應用程式資訊。
        /// 3. 資料庫有此電腦應用程式資訊則將資料更新，無則新增。
        /// 4. 新增更新成功失敗資訊資料庫。
        /// </summary>
        /// <param name="ComputerNetbiosName">電腦名稱。</param>
        /// <param name="ApplicationID">應用程式ID</param>
        /// <param name="ApplicationName">應用程式名稱。</param>
        /// <param name="MajorVersion">應用程式主要版本。</param>
        /// <param name="MinorVersion">應用程式次要版本。</param>
        /// <param name="UpdateResult">更新程式是否成功更新。</param>
        /// <param name="UpdateExitCode">更新程式結束回傳值。</param>
        /// <returns>是否成功。</returns>
        [WebMethod]
        public bool ReportUpdateVersionResult(string ComputerNetbiosName, string ApplicationID, string ApplicationName, short MajorVersion, short MinorVersion, bool UpdateResult, int UpdateExitCode)
        {
            bool ReturnResult = false;
            System.Data.SqlClient.SqlConnection _Connection = null;
            System.Data.SqlClient.SqlTransaction _Transaction = null;
            System.Data.SqlClient.SqlDataReader _Reader = null;
            System.Data.SqlClient.SqlDataAdapter _Adapter = null;
            //應用程式用戶端版本是否存在。
            bool IsExist = false;

            try
            {
                Log.TraceMethodEntry(LogSourceName,
                    "ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'; UpdateResult: '{5}'; UpdateExitCode: '{6}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion,
                    UpdateResult,
                    UpdateExitCode);

                if (ComputerNetbiosName.IndexOf('.') > 0)
                {
                    ComputerNetbiosName = ComputerNetbiosName.Substring(0, ComputerNetbiosName.IndexOf('.'));
                }

                DateTime CurrentTime = DateTime.Now;

                #region Url Decode for Call Web Service Vbs
                if (ComputerNetbiosName.IndexOf("%") >= 0)
                {
                    ComputerNetbiosName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ComputerNetbiosName)));
                }
                if (ApplicationName.IndexOf("%") >= 0)
                {
                    ApplicationName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ApplicationName)));
                }
                Log.TraceMethodEntry(LogSourceName,
                    "UrlDecode ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);
                #endregion

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();


                if ((ApplicationName == string.Empty) || (MajorVersion == short.MinValue) || (MinorVersion == short.MinValue))
                {
                    #region 取得版本資訊。

                    #region SqlCommandString
                    string SqlCommandString =
                        " SELECT DISTINCT "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion "
                        + " FROM ApplicationAutoUpdateSetting "
                        + " WHERE ApplicationID = @ApplicationID ";
                    #endregion

                    // Prepare Command
                    System.Data.SqlClient.SqlCommand Command = _Connection.CreateCommand();
                    Command.CommandTimeout = _CommandTimeout;
                    Command.CommandType = System.Data.CommandType.Text;
                    Command.CommandText = SqlCommandString;

                    #region WHERE
                    Command.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                    #endregion

                    _Reader = Command.ExecuteReader();

                    if (_Reader.Read())
                    {
                        ApplicationName = (string)_Reader["ApplicationName"];
                        MajorVersion = (short)_Reader["MajorVersion"];
                        MinorVersion = (short)_Reader["MinorVersion"];
                    }
                    _Reader.Close();
                    _Reader.Dispose();
                    _Reader = null;
                    #endregion
                }


                #region 檢查電腦應用程式資訊是否存在。

                #region IsExistSqlCommandString
                string IsExistSqlCommandString =
                    "SELECT DISTINCT "
                    + " ComputerNetbiosName, "
                    + " ApplicationID "
                    + " FROM ApplicationClientVersion"
                    + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                    + " AND ApplicationID = @ApplicationID";
                #endregion

                System.Data.SqlClient.SqlCommand IsExistCommand = _Connection.CreateCommand();
                IsExistCommand.CommandTimeout = _CommandTimeout;
                IsExistCommand.CommandType = System.Data.CommandType.Text;
                IsExistCommand.CommandText = IsExistSqlCommandString;

                #region Parameters
                IsExistCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                IsExistCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                #endregion

                _Adapter = new SqlDataAdapter(IsExistCommand);
                DataTable TempTable = new DataTable();
                if (_Adapter.Fill(TempTable) != 0)
                {
                    IsExist = true;
                }

                TempTable = null;
                #endregion


                _Transaction = _Connection.BeginTransaction(IsolationLevel.ReadCommitted);


                #region 寫入 ApplicationClientVersion

                #region SqlCommandString
                string ApplicationClientVersionSqlCommandString = string.Empty;
                if (IsExist && UpdateResult)
                {
                    // 用戶端應用程式版本資料存在，且更新成功
                    ApplicationClientVersionSqlCommandString =
                        " UPDATE ApplicationClientVersion "
                        + " SET ApplicationName = @ApplicationName, "
                        + " MajorVersion = @MajorVersion, "
                        + " MinorVersion = @MinorVersion, "
                        + " LastModifyTime = @CurrentTime, "
                        + " LastAutoUpdateHeartbeatTime = @CurrentTime "
                        + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                        + " AND ApplicationID = @ApplicationID ";
                }
                else if (IsExist && (!UpdateResult))
                {
                    // 用戶端應用程式版本資料存在，且更新不成功
                    ApplicationClientVersionSqlCommandString =
                        " UPDATE ApplicationClientVersion "
                        + " SET LastModifyTime = @CurrentTime, "
                        + " LastAutoUpdateHeartbeatTime = @CurrentTime "
                        + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                        + " AND ApplicationID = @ApplicationID ";
                }
                else if ((!IsExist) && UpdateResult)
                {
                    // 用戶端應用程式版本資料不存在，且更新成功
                    ApplicationClientVersionSqlCommandString =
                        "INSERT INTO ApplicationClientVersion ( "
                        + " ComputerNetbiosName, "
                        + " ApplicationID, "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " LastModifyTime, "
                        + " LastAutoUpdateHeartbeatTime "
                        + " ) "
                        + " VALUES ( "
                        + " @ComputerNetbiosName, "
                        + " @ApplicationID, "
                        + " @ApplicationName, "
                        + " @MajorVersion, "
                        + " @MinorVersion, "
                        + " @CurrentTime, "
                        + " @CurrentTime "
                        + " ) ";
                }
                else
                {
                    // 用戶端應用程式版本資料不存在，且更新不成功
                    ApplicationClientVersionSqlCommandString =
                        "INSERT INTO ApplicationClientVersion ( "
                        + " ComputerNetbiosName, "
                        + " ApplicationID, "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " LastModifyTime, "
                        + " LastAutoUpdateHeartbeatTime "
                        + " ) "
                        + " VALUES ( "
                        + " @ComputerNetbiosName, "
                        + " @ApplicationID, "
                        + " @ApplicationName, "
                        + " 0, "
                        + " 0, "
                        + " @CurrentTime, "
                        + " @CurrentTime "
                        + " ) ";
                }
                #endregion

                // Prepare Command
                System.Data.SqlClient.SqlCommand ApplicationClientVersionCommand = _Connection.CreateCommand();
                ApplicationClientVersionCommand.Transaction = _Transaction;
                ApplicationClientVersionCommand.CommandTimeout = _CommandTimeout;
                ApplicationClientVersionCommand.CommandType = System.Data.CommandType.Text;
                ApplicationClientVersionCommand.CommandText = ApplicationClientVersionSqlCommandString;

                #region Parameters
                ApplicationClientVersionCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = ApplicationName;
                ApplicationClientVersionCommand.Parameters.Add("@MajorVersion", SqlDbType.SmallInt).Value = MajorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@MinorVersion", SqlDbType.SmallInt).Value = MinorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@CurrentTime", SqlDbType.DateTime).Value = CurrentTime;
                #endregion

                // Execute
                if (ApplicationClientVersionCommand.ExecuteNonQuery() < 0)
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                #endregion


                #region 寫入 ApplicationClientVersionHistory

                #region SqlCommandString
                string AppicationClientVersionHistorySqlCommandString =
                    "INSERT INTO ApplicationClientVersionHistory ( "
                        + " ComputerNetbiosName, "
                        + " ApplicationID, "
                        + " DateTime, "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " UpdateResult, "
                        + " UpdateExitCode "
                        + " ) "
                        + " VALUES ( "
                        + " @ComputerNetbiosName, "
                        + " @ApplicationID, "
                        + " @DateTime, "
                        + " @ApplicationName, "
                        + " @MajorVersion, "
                        + " @MinorVersion, "
                        + " @UpdateResult, "
                        + " @UpdateExitCode "
                        + " ) ";
                #endregion

                System.Data.SqlClient.SqlCommand ApplicationClientVersionHistoryCommand = _Connection.CreateCommand();
                ApplicationClientVersionHistoryCommand.Transaction = _Transaction;
                ApplicationClientVersionHistoryCommand.CommandTimeout = _CommandTimeout;
                ApplicationClientVersionHistoryCommand.CommandType = System.Data.CommandType.Text;
                ApplicationClientVersionHistoryCommand.CommandText = AppicationClientVersionHistorySqlCommandString;

                #region Parameters
                ApplicationClientVersionHistoryCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@DateTime", SqlDbType.DateTime).Value = CurrentTime;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = ApplicationName;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@MajorVersion", SqlDbType.SmallInt).Value = MajorVersion;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@MinorVersion", SqlDbType.SmallInt).Value = MinorVersion;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@UpdateResult", SqlDbType.Bit).Value = UpdateResult;
                ApplicationClientVersionHistoryCommand.Parameters.Add("@UpdateExitCode", SqlDbType.Int).Value = UpdateExitCode;
                #endregion

                // Execute
                if (ApplicationClientVersionHistoryCommand.ExecuteNonQuery() < 0)
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                #endregion


                _Transaction.Commit();
                _Transaction = null;

                ReturnResult = true;
                return ReturnResult;

            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_ReportVersionException, e, "接收Client更新版本資訊後結果時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'; UpdateResult: '{5}'; UpdateExitCode: '{6}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion,
                    UpdateResult,
                    UpdateExitCode);
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_ReportVersionException, e, "接收Client更新版本資訊後結果時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'; UpdateResult: '{5}'; UpdateExitCode: '{6}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion,
                    UpdateResult,
                    UpdateExitCode);

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                #region Dispose
                #region Transaction.Dispose
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Rollback();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Dispose();
                    }
                }
                catch
                {
                }
                _Transaction = null;
                #endregion
                #region Reader.Dispose
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Dispose();
                    }
                }
                catch
                {
                }
                _Reader = null;
                #endregion
                #region Adapter.Dispose
                try
                {
                    if (_Adapter != null)
                    {
                        _Adapter.Dispose();
                    }
                }
                catch
                {
                }
                _Adapter = null;
                #endregion
                #region Connection.Dispose
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                    }
                }
                catch
                {
                }
                _Connection = null;
                #endregion
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region ReportLatestVersion (YesAutoUpdate定時回報版本)
        /// <summary>
        /// 接收由 YesAutoUpdate 定時回報 Client 版本資訊。
        /// 1. 應用程式名稱,版本號無值則取得最新版本資訊。
        /// 2. 檢查資料庫是否有此電腦應用程式資訊。
        /// 3. 資料庫有此電腦應用程式資訊則將資料更新，無則新增。
        /// 4. 新增資訊資料庫。
        /// </summary>
        /// <param name="ComputerNetbiosName">電腦名稱。</param>
        /// <param name="ApplicationID">應用程式ID</param>
        /// <param name="ApplicationName">應用程式名稱。</param>
        /// <param name="MajorVersion">應用程式主要版本。</param>
        /// <param name="MinorVersion">應用程式次要版本。</param>
        /// <returns>是否成功。</returns>
        [WebMethod]
        public bool ReportLatestVersion(string ComputerNetbiosName, string ApplicationID, string ApplicationName, short MajorVersion, short MinorVersion)
        {
            bool ReturnResult = false;
            System.Data.SqlClient.SqlConnection _Connection = null;
            System.Data.SqlClient.SqlTransaction _Transaction = null;
            System.Data.SqlClient.SqlDataReader _Reader = null;
            System.Data.SqlClient.SqlDataAdapter _Adapter = null;
            //應用程式用戶端版本是否存在。
            bool IsExist = false;

            try
            {
                Log.TraceMethodEntry(LogSourceName,
                    "ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);

                if (ComputerNetbiosName.IndexOf('.') > 0)
                {
                    ComputerNetbiosName = ComputerNetbiosName.Substring(0, ComputerNetbiosName.IndexOf('.'));
                }

                DateTime CurrentTime = DateTime.Now;

                #region Url Decode for Call Web Service Vbs
                if (ComputerNetbiosName.IndexOf("%") >= 0)
                {
                    ComputerNetbiosName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ComputerNetbiosName)));
                }
                if (ApplicationName.IndexOf("%") >= 0)
                {
                    ApplicationName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ApplicationName)));
                }
                Log.TraceMethodEntry(LogSourceName,
                    "UrlDecode ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);
                #endregion

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();


                if ((ApplicationName == string.Empty) || (MajorVersion == short.MinValue) || (MinorVersion == short.MinValue))
                {
                    #region 取得版本資訊。

                    #region SqlCommandString
                    string SqlCommandString =
                        " SELECT DISTINCT "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion "
                        + " FROM ApplicationAutoUpdateSetting "
                        + " WHERE ApplicationID = @ApplicationID ";
                    #endregion

                    // Prepare Command
                    System.Data.SqlClient.SqlCommand Command = _Connection.CreateCommand();
                    Command.CommandTimeout = _CommandTimeout;
                    Command.CommandType = System.Data.CommandType.Text;
                    Command.CommandText = SqlCommandString;

                    #region WHERE
                    Command.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                    #endregion

                    _Reader = Command.ExecuteReader();

                    if (_Reader.Read())
                    {
                        ApplicationName = (string)_Reader["ApplicationName"];
                        MajorVersion = (short)_Reader["MajorVersion"];
                        MinorVersion = (short)_Reader["MinorVersion"];
                    }
                    _Reader.Close();
                    _Reader.Dispose();
                    _Reader = null;
                    #endregion
                }


                #region 檢查電腦應用程式資訊是否存在。

                #region IsExistSqlCommandString
                string IsExistSqlCommandString =
                    "SELECT DISTINCT "
                    + " ComputerNetbiosName, "
                    + " ApplicationID "
                    + " FROM ApplicationClientVersion"
                    + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                    + " AND ApplicationID = @ApplicationID";
                #endregion

                System.Data.SqlClient.SqlCommand IsExistCommand = _Connection.CreateCommand();
                IsExistCommand.CommandTimeout = _CommandTimeout;
                IsExistCommand.CommandType = System.Data.CommandType.Text;
                IsExistCommand.CommandText = IsExistSqlCommandString;

                #region Parameters
                IsExistCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                IsExistCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                #endregion

                _Adapter = new SqlDataAdapter(IsExistCommand);
                DataTable TempTable = new DataTable();
                if (_Adapter.Fill(TempTable) != 0)
                {
                    IsExist = true;
                }

                TempTable = null;
                #endregion

                _Transaction = _Connection.BeginTransaction(IsolationLevel.ReadCommitted);

                #region 寫入 ApplicationClientVersion

                #region SqlCommandString
                string ApplicationClientVersionSqlCommandString = string.Empty;
                if (IsExist)
                {

                    ApplicationClientVersionSqlCommandString =
                        " UPDATE ApplicationClientVersion "
                        + " SET ApplicationName = @ApplicationName, "
                        + " MajorVersion = @MajorVersion, "
                        + " MinorVersion = @MinorVersion, "
                        + " LastAutoUpdateHeartbeatTime = @CurrentTime "
                        + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                        + " AND ApplicationID = @ApplicationID ";

                }
                else
                {
                    ApplicationClientVersionSqlCommandString =
                        "INSERT INTO ApplicationClientVersion ( "
                        + " ComputerNetbiosName, "
                        + " ApplicationID, "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " LastModifyTime, "
                        + " LastAutoUpdateHeartbeatTime "
                        + " ) "
                        + " VALUES ( "
                        + " @ComputerNetbiosName, "
                        + " @ApplicationID, "
                        + " @ApplicationName, "
                        + " @MajorVersion, "
                        + " @MinorVersion, "
                        + " @CurrentTime, "
                        + " @CurrentTime "
                        + " ) ";
                }
                #endregion

                // Prepare Command
                System.Data.SqlClient.SqlCommand ApplicationClientVersionCommand = _Connection.CreateCommand();
                ApplicationClientVersionCommand.Transaction = _Transaction;
                ApplicationClientVersionCommand.CommandTimeout = _CommandTimeout;
                ApplicationClientVersionCommand.CommandType = System.Data.CommandType.Text;
                ApplicationClientVersionCommand.CommandText = ApplicationClientVersionSqlCommandString;

                #region Parameters
                ApplicationClientVersionCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = ApplicationName;
                ApplicationClientVersionCommand.Parameters.Add("@MajorVersion", SqlDbType.SmallInt).Value = MajorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@MinorVersion", SqlDbType.SmallInt).Value = MinorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@CurrentTime", SqlDbType.DateTime).Value = CurrentTime;
                #endregion

                // Execute
                if (ApplicationClientVersionCommand.ExecuteNonQuery() < 0)
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                #endregion

                _Transaction.Commit();
                _Transaction = null;

                ReturnResult = true;
                return ReturnResult;

            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_ReportLatestVersionException, e, "接收由 YesAutoUpdate 定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_ReportLatestVersionException, e, "接收由 YesAutoUpdate 定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                #region Dispose
                #region Transaction.Dispose
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Rollback();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Dispose();
                    }
                }
                catch
                {
                }
                _Transaction = null;
                #endregion
                #region Reader.Dispose
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Dispose();
                    }
                }
                catch
                {
                }
                _Reader = null;
                #endregion
                #region Adapter.Dispose
                try
                {
                    if (_Adapter != null)
                    {
                        _Adapter.Dispose();
                    }
                }
                catch
                {
                }
                _Adapter = null;
                #endregion
                #region Connection.Dispose
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                    }
                }
                catch
                {
                }
                _Connection = null;
                #endregion
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion

        #region ReportAppliceationLatestVersion (Appliceation定時回報版本)
        /// <summary>
        /// 接收由應用程式定時回報 Client 版本資訊。
        /// 1. 應用程式名稱,版本號無值則取得最新版本資訊。
        /// 2. 檢查資料庫是否有此電腦應用程式資訊。
        /// 3. 資料庫有此電腦應用程式資訊則將資料更新，無則新增。
        /// 4. 新增資訊資料庫。
        /// </summary>
        /// <param name="ComputerNetbiosName">電腦名稱。</param>
        /// <param name="ApplicationID">應用程式ID</param>
        /// <param name="ApplicationName">應用程式名稱。</param>
        /// <param name="MajorVersion">應用程式主要版本。</param>
        /// <param name="MinorVersion">應用程式次要版本。</param>
        /// <returns>是否成功。</returns>
        [WebMethod]
        public bool ReportAppliceationLatestVersion(string ComputerNetbiosName, string ApplicationID, string ApplicationName, short MajorVersion, short MinorVersion)
        {
            bool ReturnResult = false;
            System.Data.SqlClient.SqlConnection _Connection = null;
            System.Data.SqlClient.SqlTransaction _Transaction = null;
            System.Data.SqlClient.SqlDataReader _Reader = null;
            System.Data.SqlClient.SqlDataAdapter _Adapter = null;
            //應用程式用戶端版本是否存在。
            bool IsExist = false;

            try
            {
                Log.TraceMethodEntry(LogSourceName,
                    "ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);

                if (ComputerNetbiosName.IndexOf('.') > 0)
                {
                    ComputerNetbiosName = ComputerNetbiosName.Substring(0, ComputerNetbiosName.IndexOf('.'));
                }

                DateTime CurrentTime = DateTime.Now;

                #region Url Decode for Call Web Service Vbs
                if (ComputerNetbiosName.IndexOf("%") >= 0)
                {
                    ComputerNetbiosName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ComputerNetbiosName)));
                }
                if (ApplicationName.IndexOf("%") >= 0)
                {
                    ApplicationName = Encoding.UTF8.GetString(Encoding.Convert(Encoding.GetEncoding("Big5"), Encoding.UTF8, System.Web.HttpUtility.UrlDecodeToBytes(ApplicationName)));
                }
                Log.TraceMethodEntry(LogSourceName,
                    "UrlDecode ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);
                #endregion

                // Open Connection
                _Connection = new SqlConnection(_ConnectionString);
                _Connection.Open();


                if ((ApplicationName == string.Empty) || (MajorVersion == short.MinValue) || (MinorVersion == short.MinValue))
                {
                    #region 取得版本資訊。

                    #region SqlCommandString
                    string SqlCommandString =
                        " SELECT DISTINCT "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion "
                        + " FROM ApplicationAutoUpdateSetting "
                        + " WHERE ApplicationID = @ApplicationID ";
                    #endregion

                    // Prepare Command
                    System.Data.SqlClient.SqlCommand Command = _Connection.CreateCommand();
                    Command.CommandTimeout = _CommandTimeout;
                    Command.CommandType = System.Data.CommandType.Text;
                    Command.CommandText = SqlCommandString;

                    #region WHERE
                    Command.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                    #endregion

                    _Reader = Command.ExecuteReader();

                    if (_Reader.Read())
                    {
                        ApplicationName = (string)_Reader["ApplicationName"];
                        MajorVersion = (short)_Reader["MajorVersion"];
                        MinorVersion = (short)_Reader["MinorVersion"];
                    }
                    _Reader.Close();
                    _Reader.Dispose();
                    _Reader = null;
                    #endregion
                }


                #region 檢查電腦應用程式資訊是否存在。

                #region IsExistSqlCommandString
                string IsExistSqlCommandString =
                    "SELECT DISTINCT "
                    + " ComputerNetbiosName, "
                    + " ApplicationID "
                    + " FROM ApplicationClientVersion"
                    + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                    + " AND ApplicationID = @ApplicationID";
                #endregion

                System.Data.SqlClient.SqlCommand IsExistCommand = _Connection.CreateCommand();
                IsExistCommand.CommandTimeout = _CommandTimeout;
                IsExistCommand.CommandType = System.Data.CommandType.Text;
                IsExistCommand.CommandText = IsExistSqlCommandString;

                #region Parameters
                IsExistCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                IsExistCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                #endregion

                _Adapter = new SqlDataAdapter(IsExistCommand);
                DataTable TempTable = new DataTable();
                if (_Adapter.Fill(TempTable) != 0)
                {
                    //無資料。
                    IsExist = true;
                }

                TempTable = null;
                #endregion

                _Transaction = _Connection.BeginTransaction(IsolationLevel.ReadCommitted);

                #region 寫入 ApplicationClientVersion

                #region SqlCommandString
                string ApplicationClientVersionSqlCommandString = string.Empty;
                if (IsExist)
                {

                    ApplicationClientVersionSqlCommandString =
                        " UPDATE ApplicationClientVersion "
                        + " SET ApplicationName = @ApplicationName, "
                        + " MajorVersion = @MajorVersion, "
                        + " MinorVersion = @MinorVersion, "
                        + " LastApplicationHeartbeatTime = @CurrentTime "
                        + " WHERE ComputerNetbiosName = @ComputerNetbiosName "
                        + " AND ApplicationID = @ApplicationID ";

                }
                else
                {
                    ApplicationClientVersionSqlCommandString =
                        "INSERT INTO ApplicationClientVersion ( "
                        + " ComputerNetbiosName, "
                        + " ApplicationID, "
                        + " ApplicationName, "
                        + " MajorVersion, "
                        + " MinorVersion, "
                        + " LastModifyTime, "
                        + " LastAutoUpdateHeartbeatTime, "
                        + " LastApplicationHeartbeatTime "
                        + " ) "
                        + " VALUES ( "
                        + " @ComputerNetbiosName, "
                        + " @ApplicationID, "
                        + " @ApplicationName, "
                        + " @MajorVersion, "
                        + " @MinorVersion, "
                        + " @CurrentTime, "
                        + " @CurrentTime, "
                        + " @CurrentTime "
                        + " ) ";
                }
                #endregion

                // Prepare Command
                System.Data.SqlClient.SqlCommand ApplicationClientVersionCommand = _Connection.CreateCommand();
                ApplicationClientVersionCommand.Transaction = _Transaction;
                ApplicationClientVersionCommand.CommandTimeout = _CommandTimeout;
                ApplicationClientVersionCommand.CommandType = System.Data.CommandType.Text;
                ApplicationClientVersionCommand.CommandText = ApplicationClientVersionSqlCommandString;

                #region Parameters
                ApplicationClientVersionCommand.Parameters.Add("@ComputerNetbiosName", SqlDbType.NVarChar).Value = ComputerNetbiosName;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationID", SqlDbType.NVarChar).Value = ApplicationID;
                ApplicationClientVersionCommand.Parameters.Add("@ApplicationName", SqlDbType.NVarChar).Value = ApplicationName;
                ApplicationClientVersionCommand.Parameters.Add("@MajorVersion", SqlDbType.SmallInt).Value = MajorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@MinorVersion", SqlDbType.SmallInt).Value = MinorVersion;
                ApplicationClientVersionCommand.Parameters.Add("@CurrentTime", SqlDbType.DateTime).Value = CurrentTime;
                #endregion

                // Execute
                if (ApplicationClientVersionCommand.ExecuteNonQuery() < 0)
                {
                    ReturnResult = false;
                    return ReturnResult;
                }

                #endregion

                _Transaction.Commit();
                _Transaction = null;

                ReturnResult = true;
                return ReturnResult;

            }
            catch (Exception e)
            {
                Log.LogException(LogSourceName, (int)MessageId.AutoUpdateService_ReportAppliceationLatestVersionException, e, "接收由應用程式定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);
                Log.TraceException(LogSourceName, (int)MessageId.AutoUpdateService_ReportAppliceationLatestVersionException, e, "接收由應用程式定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'",
                    ComputerNetbiosName,
                    ApplicationID,
                    ApplicationName,
                    MajorVersion,
                    MinorVersion);

                ReturnResult = false;
                return ReturnResult;
            }
            finally
            {
                #region Dispose
                #region Transaction.Dispose
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Rollback();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Transaction != null)
                    {
                        _Transaction.Dispose();
                    }
                }
                catch
                {
                }
                _Transaction = null;
                #endregion
                #region Reader.Dispose
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Reader != null)
                    {
                        _Reader.Dispose();
                    }
                }
                catch
                {
                }
                _Reader = null;
                #endregion
                #region Adapter.Dispose
                try
                {
                    if (_Adapter != null)
                    {
                        _Adapter.Dispose();
                    }
                }
                catch
                {
                }
                _Adapter = null;
                #endregion
                #region Connection.Dispose
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Close();
                    }
                }
                catch
                {
                }
                try
                {
                    if (_Connection != null)
                    {
                        _Connection.Dispose();
                    }
                }
                catch
                {
                }
                _Connection = null;
                #endregion
                #endregion

                Log.TraceMethodExit(LogSourceName, "return {0}", ReturnResult);
            }
        }
        #endregion
    }
}
