﻿using System;

namespace Yes.AutoUpdate.Web
{
    /// <summary>
    /// 訊息編號。
    /// </summary>
    public enum MessageId
    {
        #region AutoUpdateService (100xx)
        /// <summary>
        /// 取得所有應用程式最新版本資訊時發生例外錯誤。
        /// </summary>
        AutoUpdateService_GetApplicationVersionException = 10001,
        /// <summary>
        /// 取得指定應用程式ID的最新版本資訊時發生例外錯誤，ApplicationID：'{0}'。
        /// </summary>
        AutoUpdateService_GetNewVersionException = 10002,
        /// <summary>
        /// 接收Client更新版本資訊後結果時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'; UpdateResult: '{5}'; UpdateExitCode: '{6}'
        /// </summary>
        AutoUpdateService_ReportVersionException = 10003,
        /// <summary>
        /// 接收由 YesAutoUpdate 定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'
        /// </summary>
        AutoUpdateService_ReportLatestVersionException = 10004,
        /// <summary>
        /// 接收由應用程式定時回報 Client 版本資訊時發生例外錯誤。ComputerNetbiosName: '{0}'; ApplicationID: '{1}'; ApplicationName: '{2}'; MajorVersion: '{3}'; MinorVersion: '{4}'
        /// </summary>
        AutoUpdateService_ReportAppliceationLatestVersionException = 10005,
        /// <summary>
        /// 取得用戶端IP時發生例外錯誤。
        /// </summary>
        AutoUpdateService_GetRequestClientIPException = 10006,
        /// <summary>
        /// 轉換應用程式載點時發生例外錯誤。
        /// </summary>
        AutoUpdateService_ConvertDownloadPointException = 10007,
        #endregion
    }
}
