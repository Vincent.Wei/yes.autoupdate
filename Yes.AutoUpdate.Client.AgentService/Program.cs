﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Net;
using System.Net.Security;
using Yes.Framework.Helper;
using Yes.AutoUpdate.Client.AgentService.ScheduleJob;
using Yes.AutoUpdate.Client.Business;

namespace Yes.AutoUpdate.Client.AgentService
{
    static class Program
    {
        /// <summary>
        /// 追蹤來源名稱。
        /// </summary>
        private const string YesScheduleJobLogSourceName = "Yes.ScheduleJob";

        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        static void Main(string[] args)
        {
            // Ignore SSL certificate errors
            ServicePointManager.ServerCertificateValidationCallback =
               new RemoteCertificateValidationCallback(
                   delegate
                   { return true; }
               );

            #region Run from console with args
            if (args.Length > 0)
            {
                Job JobName = Job.None;

                try
                {
                    JobName = (Job)Enum.Parse(typeof(Job), args[0], false);
                }
                catch (Exception)
                {
                }

                switch (JobName)
                {
                    case Job.AutoUpdateJob:
                        {
                            #region AutoUpdateJob
                            AutoUpdateJob CurrentJob = new AutoUpdateJob();
                            CurrentJob.RunJob();
                            #endregion
                        }
                        break;
                }

                return;
            }
            #endregion


            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new SchedulingService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
