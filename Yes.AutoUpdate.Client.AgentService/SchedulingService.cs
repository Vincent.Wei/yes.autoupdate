﻿using System;
using System.ServiceProcess;
using Yes.AutoUpdate.Client.AgentService.ScheduleJob;
using Yes.Scheduling;

namespace Yes.AutoUpdate.Client.AgentService
{
    public partial class SchedulingService : ServiceBase
    {
        Yes.Scheduling.Scheduler Scheduler = null;

        #region Constructor
        public SchedulingService()
        {
            InitializeComponent();
        }
        #endregion

        #region OnStart
        protected override void OnStart(string[] args)
        {
            try
            {
                Scheduler = new Scheduler();

                Scheduler.AddJob(new AutoUpdateJob());

                Scheduler.Start();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region OnStop
        protected override void OnStop()
        {
            try
            {
                if (Scheduler != null)
                {
                    Scheduler.Stop();
                }
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
