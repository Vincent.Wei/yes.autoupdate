﻿using System;

namespace Yes.AutoUpdate.Client.AgentService
{
    public enum MessageId
    {
        #region Yes.ScheduleJob
        /// <summary>
        /// ID: {0}, Name: \"{1}\" Enabled: {2}, Schedule Time: {3}
        /// </summary>
        JobConstructorJobInformation = 101,
        /// <summary>
        /// 排程工作 {0} (\"{1}\") 建構時發生例外錯誤。
        /// </summary>
        JobConstructorException = 102,
        /// <summary>
        /// 排程工作 {0} (\"{1}\") 排程時發生例外錯誤。
        /// </summary>
        JobScheduleNextRunTimeException = 103,
        /// <summary>
        /// 排程工作 {0} (\"{1}\") 重試第 {2} 次。
        /// </summary>
        JobDoJobRetry = 104,
        /// <summary>
        /// 排程工作 {0} (\"{1}\") 執行失敗。
        /// </summary>
        JobDoJobRunFail = 105,
        /// <summary>
        /// 排程工作 {0} (\"{1}\") 執行發生例外錯誤。
        /// </summary>
        JobDoJobException = 106,
        #endregion

    }
}
