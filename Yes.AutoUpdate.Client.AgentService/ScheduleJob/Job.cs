﻿using System;

namespace Yes.AutoUpdate.Client.AgentService.ScheduleJob
{
    public enum Job
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// 自動更新應用程式排程工作。
        /// </summary>
        AutoUpdateJob = 1,
    }
}
