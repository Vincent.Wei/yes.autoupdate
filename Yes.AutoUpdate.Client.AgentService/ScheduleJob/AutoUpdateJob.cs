﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Configuration;
using System.IO;

using Yes.Framework.Helper;
using Yes.AutoUpdate.Client.Business;

namespace Yes.AutoUpdate.Client.AgentService.ScheduleJob
{
    public class AutoUpdateJob : Yes.Scheduling.BaseScheduleJob
    {
        private const string _JobName = "自動更新應用程式排程工作";

        #region RunOnStart = false
        /// <summary>
        /// 是否啟始時立即執行工作。
        /// </summary>
        private bool RunOnStart = false;
        #endregion

        #region Enabled = true
        /// <summary>
        /// 排程工作是否啟用。
        /// </summary>
        private bool Enabled = true;
        #endregion


        #region IntervalMinutes = 1
        /// <summary>
        /// 排程間隔分鐘數。
        /// </summary>
        private int IntervalMinutes = 1;
        #endregion


        #region InitialRunTimeHour = 00
        /// <summary>
        /// 排程初始同步時間(時)。
        /// </summary>
        private int InitialRunTimeHour = 00;
        #endregion

        #region InitialRunTimeMinute = 00
        /// <summary>
        /// 排程初始同步時間(分)。
        /// </summary>
        private int InitialRunTimeMinute = 00;
        #endregion

        #region InitialRunTimeSecond = 01
        /// <summary>
        /// 排程初始同步時間(秒)。
        /// </summary>
        private int InitialRunTimeSecond = 01;
        #endregion


        #region ID
        /// <summary>
        /// 取得排程工作的代碼。
        /// </summary>
        /// <value>排程工作的代碼。</value>
        public override int ID
        {
            get
            {
                return (int)Job.AutoUpdateJob;
            }
        }
        #endregion

        #region Name
        /// <summary>
        /// 取得排程工作的名稱。
        /// </summary>
        /// <value>排程工作的名稱。</value>
        public override string Name
        {
            get
            {
                return _JobName;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// 建構子。
        /// </summary>
        public AutoUpdateJob()
        {
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                string SettingString = System.Configuration.ConfigurationManager.AppSettings[Job.AutoUpdateJob.ToString()];
                string[] Settings = SettingString.Split(';');
                System.Collections.Generic.SortedDictionary<string, string> SettingDictionary = new SortedDictionary<string, string>();
                foreach (string Setting in Settings)
                {
                    string _Setting = Setting.Trim();

                    int Index = _Setting.IndexOf('=');
                    if (Index == -1)
                    {
                        continue;
                    }

                    string Key = _Setting.Substring(0, Index).Trim().ToUpper();
                    string Value = string.Empty;
                    if (_Setting.Length > Index + 1)
                    {
                        Value = _Setting.Substring(Index + 1).Trim();
                    }
                    SettingDictionary.Add(Key, Value);
                }

                string SettingValue;
                if (SettingDictionary.TryGetValue("INITIALRUNTIME", out SettingValue))
                {
                    DateTime StartTime;
                    if (!DateTime.TryParse(SettingValue, out StartTime))
                    {
                        // Random Minutes
                        Random r = new Random();
                        int RandomMinute = r.Next(0, 59);
                        int RandomSecond = r.Next(0, 59);

                        StartTime = new DateTime(
                            DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 
                            DateTime.Now.Hour, RandomMinute, RandomSecond);
                    }

                    InitialRunTimeHour = StartTime.Hour;
                    InitialRunTimeMinute = StartTime.Minute;
                    InitialRunTimeSecond = StartTime.Second;
                }
                if (SettingDictionary.TryGetValue("INTERVALMINUTES", out SettingValue))
                {
                    int.TryParse(SettingValue, out IntervalMinutes);
                }
                if (SettingDictionary.TryGetValue("ENABLED", out SettingValue))
                {
                    bool.TryParse(SettingValue, out Enabled);
                }
                if (SettingDictionary.TryGetValue("RUNONSTART", out SettingValue))
                {
                    bool.TryParse(SettingValue, out RunOnStart);
                }

                Log.Trace(LogSourceName, TraceEventType.Verbose, (int)MessageId.JobConstructorJobInformation,
                          "ID: {0}, Name: \"{1}\", Enabled: {2}, RunOnStart: {3}, InitialRunTime: {4:00}:{5:00}:{6:00}, IntervalMinutes: {7}",
                          ID, Name, Enabled, RunOnStart, InitialRunTimeHour, InitialRunTimeMinute, InitialRunTimeSecond, IntervalMinutes);

                // 初始同步時間。
                _InitialRunTime = DateTime.Today.AddHours(InitialRunTimeHour).AddMinutes(InitialRunTimeMinute).AddSeconds(InitialRunTimeSecond);

                // 一定要立即排程下次執行時間，否則排程永遠不會執行。
                _NextRunTime = ScheduleNextRunTime();

                if (RunOnStart)
                {
                    // 起始時立即執行排程工作。
                    _NextRunTime = _NextRunTime.AddMinutes(-IntervalMinutes);
                }
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.JobConstructorException, e, "排程工作 {0} (\"{1}\") 建構時發生例外錯誤。", ID, Name);
                Log.LogException(LogSourceName, (int)MessageId.JobConstructorException, e, "排程工作 {0} (\"{1}\") 建構時發生例外錯誤。", ID, Name);

                throw;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return");
            }
        }
        #endregion

        #region ScheduleNextRunTime
        /// <summary>
        /// 排程下次執行時間。
        /// </summary>
        /// <returns>排程下次執行時間。</returns>
        protected override DateTime ScheduleNextRunTime()
        {
            Nullable<DateTime> NextTime = null;

            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                NextTime = ScheduleInterval(IntervalMinutes);

                return NextTime.Value;
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.JobScheduleNextRunTimeException, e, "排程工作 {0} (\"{1}\") 排程時發生例外錯誤。", ID, Name);
                Log.LogException(LogSourceName, (int)MessageId.JobScheduleNextRunTimeException, e, "排程工作 {0} (\"{1}\") 排程時發生例外錯誤。", ID, Name);

                throw;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0:yyyy/MM/dd HH:mm:ss.fff}", NextTime);
            }
        }
        #endregion

        #region DoJob
        /// <summary>
        /// 實際排程工作內容。
        /// </summary>
        protected override void DoJob()
        {
            try
            {
                Log.TraceMethodEntry(LogSourceName, string.Empty);

                if (!Enabled)
                {
                    return;
                }

                #region 執行程式
                string ApplicationListFileName = GetRealPath(ConfigurationManager.AppSettings["ApplicationListFileName"]);

                AutoUpateProcess ProcessAutoUpate = new AutoUpateProcess();
                if (!ProcessAutoUpate.AutoUpdate(ApplicationListFileName))
                {
                    Log.LogError(LogSourceName, (int)MessageId.JobDoJobRunFail, "排程工作 {0} (\"{1}\") 執行失敗。", ID, Name);
                    Log.TraceError(LogSourceName, (int)MessageId.JobDoJobRunFail, "排程工作 {0} (\"{1}\") 執行失敗。", ID, Name);
                }
                ProcessAutoUpate = null;
                #endregion
            }
            catch (Exception e)
            {
                Log.TraceException(LogSourceName, (int)MessageId.JobDoJobException, e, "排程工作 {0} (\"{1}\") 執行發生例外錯誤。", ID, Name);
                Log.LogException(LogSourceName, (int)MessageId.JobDoJobException, e, "排程工作 {0} (\"{1}\") 執行發生例外錯誤。", ID, Name);
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, string.Empty);
            }
        }
        #endregion


        #region GetRealPath
        /// <summary>
        /// 轉換系統環境參數，以及相對路徑，取得真實且完整的檔案系統路徑。
        /// </summary>
        /// <param name="OriginalPath">原始路徑。</param>
        /// <returns>真實且完整的檔案系統路徑。</returns>
        public static string GetRealPath(string OriginalPath)
        {
            string ResultPath = string.Empty;

            try
            {
                Log.TraceMethodEntry(LogSourceName, "OriginalPath: \"{0}\"", OriginalPath);

                if (OriginalPath.IndexOf("%") >= 0)
                {
                    string TempPath = OriginalPath;
                    while (TempPath.IndexOf("%") >= 0)
                    {
                        string EnvironmentVariable = string.Empty;
                        int FirstIndex = TempPath.IndexOf("%");
                        int SecondIndex = TempPath.IndexOf("%", FirstIndex + 1);
                        if (SecondIndex > FirstIndex)
                        {
                            EnvironmentVariable = TempPath.Substring(FirstIndex, SecondIndex - FirstIndex + 1);
                        }
                        TempPath = TempPath.Replace(EnvironmentVariable, Environment.GetEnvironmentVariable(EnvironmentVariable.Replace("%", "")));
                    }
                    ResultPath = TempPath;
                }
                else
                {
                    ResultPath = OriginalPath;
                }

                if (ResultPath.StartsWith(".\\")
                    || ResultPath.StartsWith("..\\")
                    || (ResultPath.StartsWith("\\") && (!ResultPath.StartsWith("\\\\")))
                    || (ResultPath.IndexOf(":") < 0 && (!ResultPath.StartsWith("\\\\"))))
                {
                    string CurrentPath = new FileInfo(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName).Directory.FullName;
                    if (!CurrentPath.EndsWith("\\") || !CurrentPath.EndsWith("/"))
                    {
                        CurrentPath += "\\";
                    }

                    if (ResultPath.StartsWith(".\\"))
                    {
                        ResultPath = CurrentPath + ResultPath.Substring(2);
                    }
                    else if (ResultPath.StartsWith("..\\"))
                    {
                        ResultPath = (new DirectoryInfo(CurrentPath).Parent.FullName) + ResultPath.Substring(2);
                    }
                    else if (ResultPath.StartsWith("\\"))
                    {
                        ResultPath = CurrentPath + ResultPath.Substring(1);
                    }
                    else
                    {
                        ResultPath = CurrentPath + ResultPath;
                    }
                }

                return ResultPath;
            }
            catch (Exception e)
            {
                ResultPath = string.Empty;
                throw e;
            }
            finally
            {
                Log.TraceMethodExit(LogSourceName, "return {0}", ResultPath);
            }
        }
        #endregion
    }
}