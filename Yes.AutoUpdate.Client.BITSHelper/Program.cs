﻿using System.Collections.Generic;
using SharpBits.Base;

namespace Yes.AutoUpdate.Client.BITSHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            BitsManager manager = new BitsManager();

            SortedList<string, int> UserJobCounts = new SortedList<string, int>();

            int TotalJobs = 0;
            manager.EnumJobs();
            foreach (BitsJob job in manager.Jobs.Values)
            {
                TotalJobs++;

                System.Console.WriteLine("Id: {0}", job.JobId);
                System.Console.WriteLine("Name: {0}", job.DisplayName);
                System.Console.WriteLine("Description: {0}", job.Description);
                System.Console.WriteLine("Owner: {0} ({1})", job.Owner, job.OwnerName);
                if (!UserJobCounts.ContainsKey(job.OwnerName))
                {
                    UserJobCounts.Add(job.OwnerName, 0);
                }
                UserJobCounts[job.OwnerName]++;

                System.Console.WriteLine("Type: {0}", job.JobType);
                System.Console.WriteLine("State: {0}", job.State);
                System.Console.WriteLine("Priority: {0}", job.Priority);
                //System.Console.WriteLine("MaximumDownloadTime: {0}", job.MaximumDownloadTime);
                System.Console.WriteLine("MinimumRetryDelay: {0}", job.MinimumRetryDelay);
                System.Console.WriteLine("NoProgressTimeout: {0}", job.NoProgressTimeout);
                //System.Console.WriteLine("PeerCachingFlags: {0}", job.PeerCachingFlags);

                System.Console.WriteLine("CreationTime: {0:yyyy/MM/dd HH:mm:ss}", job.JobTimes.CreationTime);
                System.Console.WriteLine("ModificationTime: {0:yyyy/MM/dd HH:mm:ss}", job.JobTimes.ModificationTime);
                System.Console.WriteLine("TransferCompletionTime: {0:yyyy/MM/dd HH:mm:ss}", job.JobTimes.TransferCompletionTime);

                System.Console.WriteLine("Progress Bytes Transferred / Total: {0} / {1}", job.Progress.BytesTransferred, job.Progress.BytesTotal);
                System.Console.WriteLine("Progress Files Transferred / Total: {0} / {1}", job.Progress.FilesTransferred, job.Progress.FilesTotal);

                job.EnumFiles();
                System.Console.WriteLine("File Count: {0}", job.Files.Count);
                foreach (BitsFile file in job.Files)
                {
                    System.Console.WriteLine("  File Name: {0} ({1})", file.LocalName, file.RemoteName);
                    System.Console.WriteLine("  Bytes Transfered / Total: {0} / {1}", file.Progress.BytesTransferred, file.Progress.BytesTotal);
                    //System.Console.WriteLine(" File Completed: {0}", file.Progress.Completed);
                }

                System.Console.WriteLine("Error Count: {0}", job.ErrorCount);
                BitsError error = job.Error;
                if (null != error)
                {
                    System.Console.WriteLine("  Error Code: {0}", error.ErrorCode);
                    System.Console.WriteLine("  Error Description: {0}", error.Description);
                    System.Console.WriteLine("  Error Context: {0}", error.ErrorContext);
                    System.Console.WriteLine("  Error Context Description: {0}", error.ContextDescription);
                    System.Console.WriteLine("  Error Protocol: {0}", error.Protocol);
                }

                System.Console.WriteLine("=====");
                System.Console.WriteLine(string.Empty);
            }


            System.Console.WriteLine(string.Empty);
            foreach (string JobOwner in UserJobCounts.Keys)
            {
                System.Console.WriteLine("{0} Jobs: {1}", JobOwner, UserJobCounts[JobOwner]);
            }

            System.Console.WriteLine(string.Empty);
            System.Console.WriteLine("Total Jobs: {0}", TotalJobs);
            System.Console.WriteLine(string.Empty);
        }
    }
}
